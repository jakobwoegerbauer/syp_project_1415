package graphics;
import com.jme3.app.SimpleApplication;

/**
 * Created by Jakob on 02.03.2015.
 */
public class Main extends SimpleApplication {
    public static void main(String[] args){
        Main app = new Main();
        app.start();
    }

    @Override
    public void simpleInitApp() {

    }

    @Override
    public void update() {
        super.update();
    }
}
