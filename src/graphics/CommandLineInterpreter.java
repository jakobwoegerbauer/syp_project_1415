package graphics;

import com.jme3.app.SimpleApplication;
import com.jme3.collision.CollisionResults;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.light.DirectionalLight;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Ray;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.system.AppSettings;
import logic.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;


/**
 * Created by Philipp on 27.04.2015.
 */
public class CommandLineInterpreter extends SimpleApplication{
    private static String HELPLAMMER = "syp_project_1415/assets/XML/TestLevel/testLevel.xml";
    private static String input;
    private static Level level;
    private static Float levelSpeed = 1f;
    private static List<Player> playerList;
    private static List<Unit> unitList = new LinkedList<>();
    private static List<UnitGroup> unitGroupsList = new LinkedList<>();
    private static List<Vehicle> vehicleList = new LinkedList<>();
    private static List<Infantry> infantryList = new LinkedList<>();
    private static Unit selected;

    public static void main(String[] args)
    {
        CommandLineInterpreter cmi = new CommandLineInterpreter();
        cmi.setDisplayStatView(false);
        cmi.setShowSettings(false);
        cmi.setPauseOnLostFocus(true);
        cmi.setDisplayFps(false);

        AppSettings settings = new AppSettings(true);
        settings.setFullscreen(false);
        settings.setResolution(1600, 900);
        settings.setTitle("MilitaryStrategyEngine0.6");

        cmi.setSettings(settings);
        cmi.start();
    }

    @Override
    public void simpleInitApp() {
        level = Level.load(HELPLAMMER, getAssetManager());

        getCamera().setLocation(new Vector3f(4f,1.5f,2f));
        getCamera().lookAt(new Vector3f(-7,2,-7),Vector3f.UNIT_Y);

        playerList = level.getPlayers();
        Weapon w = level.getPlayers().get(0).getInfantries().get(0).getActiveWeapon();
        refresh();


        DirectionalLight sun = new DirectionalLight();
        sun.setDirection((new Vector3f(-0.5f, -0.5f, -0.5f)).normalizeLocal());
        sun.setColor(ColorRGBA.White);
        rootNode.addLight(sun);

        getFlyByCamera().setMoveSpeed(10);

        rootNode.attachChild(level.getNode());

        inputManager.addMapping("pick target", new MouseButtonTrigger(MouseInput.BUTTON_LEFT)); // trigger 2: left-button click
        inputManager.addListener((AnalogListener) (name, intensity, tpf) -> {
            if (name.equals("pick target")) {
                // Reset results list.
                CollisionResults results = new CollisionResults();
                // Convert screen click to 3d position
                Vector2f click2d = inputManager.getCursorPosition();
                Vector3f click3d = cam.getWorldCoordinates(new Vector2f(click2d.x, click2d.y), 0f).clone();
                Vector3f dir = cam.getWorldCoordinates(new Vector2f(click2d.x, click2d.y), 1f).subtractLocal(click3d);
                // Aim the ray from the clicked spot forwards.
                Ray ray = new Ray(click3d, dir);
                // Collect intersections between ray and all nodes in results list.
                rootNode.collideWith(ray, results);
                // (Print the results so we see what is going on:)
                for (int i = 0; i < results.size(); i++) {
                    // (For each "hit", we know distance, impact point, geometry.)
                    float dist = results.getCollision(i).getDistance();
                    Vector3f pt = results.getCollision(i).getContactPoint();
                    String target = results.getCollision(i).getGeometry().getName();
                    System.out.println("Selection #" + i + ": " + target + " at " + pt + ", " + dist + " WU away.");
                }
                // Use the results -- we rotate the selected geometry.
                if (results.size() > 0) {
                    // The closest result is the target that the player picked:
                    Geometry target = results.getClosestCollision().getGeometry();
                    for(Unit u : unitList){
                        if(u.getModel().getChildren().contains(target)){
                            selected = u;
                            System.out.println("selected: " + u.toString() + " " + u.getName());
                            break;
                        }
                    }
                }
            }
        }, "pick target");

        Thread t = new Thread(() -> {
            try {
                Thread.sleep(2000);
                playerList.get(0).getUnitGroups().get(0).moveTo(new Vector3f(-7, 0, 6));
                Thread.sleep(2500);
                playerList.get(0).getUnitGroups().get(1).getLeader().driveTo(playerList.get(1).getVehicles().get(0), playerList.get(1).getVehicles().get(0).getPosition());

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        t.start();

    }


    public static void refresh()
    {
        unitList = new LinkedList<>();
        unitGroupsList = new LinkedList<>();
        vehicleList = new LinkedList<>();
        infantryList = new LinkedList<>();
        for(Player p : playerList){
            p.getUnitGroups().forEach(unitGroup -> unitList.addAll(unitGroup.getUnits()));
            unitGroupsList.addAll(p.getUnitGroups());
        }

        for(Unit u : unitList)
        {
            if(u instanceof Infantry)
            {
                infantryList.add((Infantry)u);
            }

            else if(u instanceof Vehicle)
            {
                vehicleList.add((Vehicle)u);
            }
        }
    }

    //Getter und Setter//

    public static String getInput() {
        return input;
    }

    public static void setInput(String input) {
        CommandLineInterpreter.input = input;
    }

    public static boolean interprete(String input){ //
        setInput(input);
        refresh();

        if(getInput() == null) {
            return false;
        }


        try {
            if (getInput().toLowerCase().contains("help")) {
                help();
            }//help

            if (getInput().contains("(")) {
                String[] splitted = getInput().split("\\(");
                if (getInput().contains(",")) {
                    String[] splitagain = splitted[1].split(",");

                    switch (splitted[0].toLowerCase()) {
                        case "move":
                            Infantry foundInf = null;
                            for(Infantry i : infantryList)
                            {
                                if(i.getName().equals(splitagain[0]))
                                {
                                    foundInf = i;
                                    break;
                                }
                            }

                            if(foundInf == null) {
                                throw new RuntimeException("Unit not found");
                            }
                            else {
                                move(foundInf, Float.parseFloat(splitagain[1]), Float.parseFloat(splitagain[2]));
                            }

                            break;

                        case "setinventory": //setInventory(Unit unit, Inventoryitem item, give/remove)
                            Unit insertUn = null;

                            for(Unit u : unitList)
                            {
                                if(u.getName().equals(splitagain[0])) {
                                    insertUn = u;
                                    break;
                                }
                            }

                            List<InventoryItem> availableItems = new LinkedList<>();
                            InventoryItem insertInv = null;

                            for(InventoryItem i : availableItems)
                            {
                                if(i.getName().equals(splitagain[1])) {
                                    insertInv = i;
                                    break;
                                }
                            }

                            if(insertInv == null || insertUn == null) {
                                throw new RuntimeException("Fehlerhafte eingabe!");
                            }

                            if(splitagain[3].toLowerCase().equals("give") || splitagain[3].toLowerCase().equals("remove"))
                            {
                               setInventory(insertUn,insertInv, splitagain[3].toLowerCase() == "give");
                            }
                            break;

                        case "searchenemy": // searchEnemy(Unitgroup unitgroup) returns (nearest) Unitgroup

                            for(UnitGroup u : unitGroupsList)
                            {
                                if(u.getName() == splitagain[0]) {
                                    searchEnemy(u);
                                    break;
                                }
                            }
                            break;

                        case "setspeed": // setSpeed(Integer speed) 0-3
                            setSpeed(Integer.parseInt(splitted[1]));
                            break;

                        case "quest": // quest() lists quests
                            quest();
                            break;


                        case "entervehicle": //enterVehicle(Infantry infantry, Vehicle vehicle)
                            Infantry infantry = null;
                            Vehicle vehicle = null;

                            for(Infantry i : infantryList)
                            {
                                if(i.getName().equals(splitagain[0])) {
                                    infantry = i;
                                    break;
                                }
                            }

                            for(Vehicle v : vehicleList)
                            {
                                if(v.getName().equals(splitagain[1])){
                                    vehicle = v;
                                    break;
                                }
                            }
                            if(vehicle == null || infantry == null) {
                                throw new RuntimeException("Vehicle or Infantry is null");
                            }

                            else {
                                enterVehicle(infantry, vehicle);
                            }
                            break;

                        case "leavevehicle": //leaveVehicle(Infantry infantry, actual vehicle)
                            Infantry x = null;
                            for(Infantry i : infantryList)
                            {
                                if(i.getName().equals(splitagain[0])) {
                                    x = i;
                                    break;
                                }
                            }

                            if(x != null) {
                                leaveVehicle(x);
                            }

                            else
                                throw new RuntimeException("Infantry guy not found");

                            break;
                    }
                }
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private static void move(Infantry i, float x, float y)
    {
        i.moveTo(new Vector3f(x, 0, y));
    }

    private static void quest() {
        String output = level.getLevelName() + "\n" + level.getDescription();
        CommandLine.addDescriptionLine(output);
    }

    private static void setSpeed(float i) {
        levelSpeed = i;
    }

    private static void searchEnemy(UnitGroup unitGroup) {
        refresh();
        unitGroup.searchForEnemies();
    }

    private static void help() throws IOException {
        List<String> lines = Files.readAllLines(Paths.get("assets/gui/help.txt"));

        for(String s : lines) {
            CommandLine.addDescriptionLine(s);
        }

    }

    @Override
    public void simpleUpdate(float tpf) {
        super.simpleUpdate(tpf);
        level.update(tpf * levelSpeed);
    }

    private static void setInventory(Unit unit, InventoryItem item, boolean give){
        refresh();
        if(give) {
            unit.addItem(item);
        }
        else {
            unit.removeItem(item, 1);
        }
    }

    private static void enterVehicle(Infantry infantry, Vehicle vehicle)
    {
        infantry.driveTo(vehicle, vehicle.getPosition());
        refresh();
    }

    private static void leaveVehicle(Infantry infantry)
    {
        infantry.getVehicle().releaseDriver();
    }
}