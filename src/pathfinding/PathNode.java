package pathfinding;

/**
 * Created by Jakob on 13.02.2015.
 */
public class PathNode {

    private PathNode predecessor;
    private double f;
    private double g;
    private NavTriangle triangle;

    public PathNode(NavTriangle triangle){
        this.triangle = triangle;
    }

    public PathNode getPredecessor() {
        return predecessor;
    }

    public void setPredecessor(PathNode predecessor) {
        this.predecessor = predecessor;
    }

    public double getF() {
        return f;
    }

    public void setF(double f) {
        this.f = f;
    }


    public NavTriangle getTriangle() {
        return triangle;
    }

    public void setG(double val) {
        this.g = val;
    }

    public double getG() {
        return g;
    }
}
