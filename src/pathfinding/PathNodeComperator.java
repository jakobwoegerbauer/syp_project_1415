package pathfinding;

import java.util.Comparator;

/**
 * Created by Jakob on 13.02.2015.
 */
public class PathNodeComperator implements Comparator<PathNode> {

    @Override
    public int compare(PathNode o1, PathNode o2) {
        return ((Double)o1.getF()).compareTo(o2.getF());
    }
}
