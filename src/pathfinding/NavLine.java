package pathfinding;

/**
 * Created by Jakob on 19.02.2015.
 */
public class NavLine {
    private int p1;
    private int p2;

    public NavLine(int p1, int p2){
        this.setP1(p1);
        this.setP2(p2);
    }

    public int getP1() {
        return p1;
    }

    public void setP1(int p1) {
        this.p1 = p1;
    }

    public int getP2() {
        return p2;
    }

    public void setP2(int p2) {
        this.p2 = p2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NavLine navLine = (NavLine) o;

        if((p1 == navLine.p1 && p2 == navLine.p2) || (p1 == navLine.p2 && p2 == navLine.p1)){
            return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        int result = p1;
        result = 31 * result + p2;
        return result;
    }
}
