package pathfinding;

import com.jme3.app.SimpleApplication;
import com.jme3.asset.plugins.ClasspathLocator;
import com.jme3.niftygui.NiftyJmeDisplay;
import com.jme3.scene.Node;
import com.jme3.system.AppSettings;
import com.jme3.terrain.Terrain;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;

/**
 * Created by Jakob on 15.01.2015.
 */
// implements ScreenController
public class MapEditor extends SimpleApplication implements ScreenController {

    boolean first = true;
    Node menuNode;
    Nifty nifty;
    private Terrain terrain;
    private Node terrainNode;

    public static void main(String[] args){
        MapEditor m = new MapEditor();
        m.setDisplayStatView(false);

        AppSettings cfg = new AppSettings(true);
        cfg.setFrameRate(60); // set to less than or equal screen refresh rate
        cfg.setVSync(true);   // prevents page tearing
        cfg.setFrequency(60); // set to screen refresh rate
        cfg.setResolution(1024, 700);
        cfg.setFullscreen(false);
        cfg.setSamples(2);    // anti-aliasing
        cfg.setTitle("MapEditor 1.0.0"); // branding: window name*/

        //cfg.setSettingsDialogImage("Interface/MySplashscreen.png");
        m.setShowSettings(false); // or don't display splashscreen
        //m.setSettings(cfg);


        m.start();
    }


    @Override
    public void simpleInitApp() {

        NiftyJmeDisplay niftyDisplay = new NiftyJmeDisplay(assetManager,
                inputManager,
                audioRenderer,
                guiViewPort);
        nifty = niftyDisplay.getNifty();

        // WARNING: Hack alert. This is needed so the fonts' textures can be found
        assetManager.registerLocator("/tutorial/fonts/", ClasspathLocator.class.getName());



        // load helloworld.xml
        //nifty.fromXml("jme3test/niftygui/helloworld.xml", "start");
        nifty.fromXml("gui/mapEditor.xml", "mapGui");

        // attach the nifty display to the gui view port as a processor
        guiViewPort.addProcessor(niftyDisplay);


        // disable the fly cam
        flyCam.setEnabled(false);

        // allow us to interact with gui components
        inputManager.setCursorVisible(true);
    }

    public void importTerrain(){
        /*System.out.println("Import terrain");
        FileDialog fileDialog = new FileDialog(new Frame(), "Select terrain");
        fileDialog.setVisible(true);

        System.out.println(fileDialog.getDirectory() + "" + fileDialog.getFile());

        File f = new File(fileDialog.getDirectory() + "" + fileDialog.getFile());
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(f);

            BinaryImporter imp = BinaryImporter.getInstance();
            imp.setAssetManager(super.getStateManager().getApplication().getAssetManager());
            terrainNode = (Node)imp.load(new BufferedInputStream(fis));

        } catch (Exception e) {
            e.printStackTrace();
        }

       // Spatial myTerrain = (Spatial)myManager.loadAsset(fileDialog.getDirectory() + "" + fileDialog.getFile());
        //rootNode.attachChild(terrainNode);
        try {
            terrain = (TerrainQuad) terrainNode;
        }catch (Exception e){
            e.printStackTrace();
        }


        /* A colored lit cube. Needs light source!
        Box boxMesh = new Box(1f,1f,1f);
        Geometry boxGeo = new Geometry("Colored Box", boxMesh);
        Material boxMat = new Material(assetManager, "Common/MatDefs/Light/Lighting.j3md");
        boxMat.setBoolean("UseMaterialColors", true);
        boxMat.setColor("Ambient", ColorRGBA.Green);
        boxMat.setColor("Diffuse", ColorRGBA.Green);
        boxGeo.setMaterial(boxMat);
        rootNode.attachChild(boxGeo);

        /** A white, directional light source
        DirectionalLight sun = new DirectionalLight();
        sun.setDirection((new Vector3f(-0.5f, -0.5f, -0.5f)).normalizeLocal());
        sun.setColor(ColorRGBA.White);
        rootNode.addLight(sun);*/

    }

    @Override
    public void update() {
        super.update();
        if(first){
            importTerrain();
        }
        first = false;
    }

    @Override
    public void bind(Nifty nifty, Screen screen) {

    }

    @Override
    public void onStartScreen() {

    }

    @Override
    public void onEndScreen() {

    }
}
