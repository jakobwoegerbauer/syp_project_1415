package pathfinding;

import com.jme3.math.Vector3f;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Jakob on 10.02.2015.
 */
    public class NavTriangle {
    private List<NavTriangle> neighbours;
    private Integer[] points;

    private int id;

    public List<NavTriangle> getNeighbours() {
        return new LinkedList<>(neighbours);
    }

    public NavTriangle(int id) {
        this.id = id;
        points = new Integer[3];
        neighbours = new LinkedList<NavTriangle>();
    }

    public void addNeighbour(NavTriangle neighbour) {
        this.neighbours.add(neighbour);
    }

    public void setPoints(int i1, int i2, int i3) {
        points = new Integer[3];
        points[0] = i1;
        points[1] = i2;
        points[2] = i3;
    }

    public void setNeighbours(List<NavTriangle> n) {
        this.neighbours = n;
    }

    public void setPoints(Integer[] points) {
        points = points;
    }

    public int getId() {
        return id;
    }


    public static Vector3f getMiddle(Vector3f[] points) {
        Vector3f sum = points[0].add(points[1]).add(points[2]);
        return sum.divide(3);
    }

    public Integer[] getPoints() {
        return points;
    }

    public boolean containsPoint(int id) {
        return points[0] == id || points[1] == id || points[2] == id;
    }
}
