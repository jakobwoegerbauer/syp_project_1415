package pathfinding;


import com.jme3.math.Ray;
import com.jme3.math.Triangle;
import com.jme3.math.Vector3f;
import com.jme3.scene.Mesh;
import org.omg.CORBA.NamedValue;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.*;
import java.util.stream.Collectors;

public class PathFinder {
    private NavigationMesh navigationMesh;
    private boolean initialized = false;

    /**
     * Initializes the PathFinding algorithm.
     * initialize has to be called before any pathfinding.
     * @param navMesh the navigationMesh used for pathfinding.
     */
    public void initialize(Mesh navMesh) {
        navigationMesh = new NavigationMesh(navMesh);
        navigationMesh.initialize();
        initialized = true;
    }

    /**
     * Finds the fastest way to the target position. When the best way is found, the callback-method gets called.
     * @param start actual position of the unit.
     * @param target target position.
     * @param callback class with callback method.
     * @param data Object for saving custom information in this query.
     */
    public void getInfantryPath(Vector3f start, Vector3f target, PathFindingCallback callback, Object data) {
        if(!initialized){
            throw new UnsupportedOperationException("initialized has to be called before.");
        }

        PathFinderThread t = new PathFinderThread(start, target, callback, data);
        t.run();
    }

     /**
     * Finds the fastest way to the target position. When the best way is found, the callback-method gets called.
     * At the moment there is no difference to getInfantryPath(Vector3f start, Vector3f target, PathFindingCallback callback, Object data)
     * @param start actual position of the unit.
     * @param target target position.
     * @param callback class with callback method.
     * @param data Object for saving custom information in this query.
     */
    public void getVehiclePath(Vector3f start, Vector3f target, PathFindingCallback callback, Object data) {
        if(!initialized){
            throw new UnsupportedOperationException("initialized has to be called before.");
        }

        PathFinderThread t = new PathFinderThread(start, target, callback, data);
        t.run();
    }

    private class PathFinderThread implements Runnable{
        private PathFindingCallback callback;
        private Vector3f start;
        private Vector3f end;
        private Object data;

        public PathFinderThread(Vector3f start, Vector3f end, PathFindingCallback callback, Object data) {
            this.callback = callback;
            this.start = start;
            this.end = end;
            this.data = data;
        }

        @Override
        public void run() {
            PathNode s = new PathNode(navigationMesh.getTriangle(start));
            PathNode t = new PathNode(navigationMesh.getTriangle(end));

            callback.pathFound(getShortenedPath(start, end, getTrianglePath(s, t)), data);
        }

        /**
         * Returns a list of waypoints in the correct order by using the centroids of the triangles.
         * @param start actual position of the unit.
         * @param target target position.
         * @param nodes list of PathNodes with the shortest path.
         * @return list of waypoints.
         */
        private List<Vector3f> getPositions(Vector3f start, Vector3f target, List<PathNode> nodes) {
            List<Vector3f> result = new LinkedList<>();
            result.add(start);
            result.addAll(nodes.stream().map(n -> NavTriangle.getMiddle(navigationMesh.getPoints(n.getTriangle()))).collect(Collectors.toList()));
            result.add(target);
            return result;
        }

        /**
         * Runs an A* algorithm to find the shortest path to the target through the navigation mesh.
         * @param start actual position of the unit.
         * @param end target position.
         * @return list of PathNodes with the shortest path.
         */
        private List<PathNode> getTrianglePath(PathNode start, PathNode end) {
            PriorityQueue<PathNode> openList = new PriorityQueue<>(new PathNodeComperator());
            Map<Integer, PathNode> closedList = new HashMap<>();
            PathNode currentNode = start;

            openList.add(end);

            do {
                currentNode = openList.remove();

                if (currentNode.getTriangle().equals(start.getTriangle())) {
                    return calcPath(currentNode);
                }

                closedList.put(currentNode.getTriangle().getId(), currentNode);
                expandNode(currentNode,openList,closedList,end);

            } while (!openList.isEmpty());
            return null;
        }

        /***
         * Returns a list of PathNodes, which the shortest way goes through.
         * @param end target position
         * @return list of PathNodes with the shortest path.
         */
        private List<PathNode> calcPath(PathNode end) {
            List<PathNode> path = new LinkedList<>();
            path.add(end);

            while (end.getPredecessor() != null) {
                end = end.getPredecessor();
                path.add(end);
            }

            return path;
        }

        /***
         * Part of the A* algorithm.
         * @param currentNode actual node.
         * @param openList list with incomplete nodes.
         * @param closedList list with used and unnecessary nodes.
         * @param end target position
         */
        private void expandNode(PathNode currentNode, Queue<PathNode> openList, Map<Integer, PathNode> closedList, PathNode end) {
            for (NavTriangle nt : currentNode.getTriangle().getNeighbours()) {
                PathNode t = new PathNode(nt);
                if (closedList.containsKey(t.getTriangle().getId())) {
                    continue;
                }

                double g = currentNode.getG() + navigationMesh.getGValue(currentNode,t);
                if (openList.contains(nt) && g >= t.getG()) {
                    continue;
                }

                t.setPredecessor(currentNode);
                t.setG(g);

                t.setF(g + (navigationMesh.getMiddle(t.getTriangle()).subtract(navigationMesh.getMiddle(end.getTriangle()))).length());
                openList.add(t);
                if (!openList.contains(t)) {
                    openList.add(t);
                }
            }
        }

        /***
         * Calculates the shortest path through the triangles calculated before.
         * @param start actual position of the unit.
         * @param end target position.
         * @param triangles list of triangles where the shortest path goes through.
         * @return a list of points which describe the shortest path.
         */
        private List<Vector3f> getShortenedPath(Vector3f start, Vector3f end, List<PathNode> triangles){

            int startPoint = navigationMesh.addPointTemporarily(start);
            int endPoint = navigationMesh.addPointTemporarily(end);

            List<Integer> points = new LinkedList<>();
            List<NavTriangle> tList = new LinkedList<>();

            points.add(startPoint);
            for(PathNode n : triangles){
                tList.add(n.getTriangle());
                points.add(navigationMesh.addPointTemporarily(NavTriangle.getMiddle(navigationMesh.getPoints(n.getTriangle()))));
            }
            points.add(endPoint);

            List<Vector3f> vectors = navigationMesh.getPoints(points);
            for(Vector3f v : vectors){
                v.setY(0);
            }
            return vectors;

            /*List<NavTriangle> actTriangles = new LinkedList<>();

            int actPoint = startPoint;
            int actPointIndex = 0;
            int triangleStartIndex = 0;
            int triangleEndIndex = 0;

            for(int i = 0; i < triangles.size(); i++){
                actTriangles.add(tList.get(i));
            }
            while(actPoint != points.get(points.size()-1) && actPointIndex < points.size()-1){
                int innerPoint = getCollisionPoint(actTriangles, actPoint, points.get(actPointIndex + 1));
                if(innerPoint == -1){
                    //direct route is possible -> remove point between the other two points. Add a the next triangle for calculation.
                    triangleEndIndex++;
                    if(triangleEndIndex >= triangles.size()){
                        break;
                    }
                    actTriangles.add(tList.get(triangleEndIndex));
                }else{
                    //direct way is not possible. Add the inner point of the last triangle and try the next point as start point for a direct route.

                    //points.add(actPointIndex + 1, lastPointRemoved);
                    points.add(actPointIndex + 1,innerPoint);

                    actPointIndex++;
                    actPoint = points.get(actPointIndex);
                    triangleEndIndex--;
                    triangleStartIndex = triangleEndIndex + actTriangles.size()-1;
                }
            }
            return navigationMesh.getPoints(points);*/
        }

        /**
         * Returns true if a direct route between start and end is possible, without leaving the triangles.
         * All Z-Vars are set to 0, so it's a 2-dimensional calculation
         * @param triangles list of triangles.
         * @param start index of the start-point.
         * @param end index of the end-point.
         * @return -1 if route is possible, index of "problematic" triangle if route is not possible.
         */
        private int getCollisionPoint(List<NavTriangle> triangles, int start, int end){
            Vector3f sCopy = navigationMesh.getPoint(start).setY(0);
            Vector3f eCopy = navigationMesh.getPoint(end).setY(0);
            Ray r = new Ray(sCopy, eCopy.subtract(sCopy).normalize());
            List<NavLine> allCollisions = new LinkedList<>();

            List<NavLine> oldCollisions = null;

            for(int i = 0; i < triangles.size()-1; i++){
                List<NavLine> collisions = getCollisionSides(triangles.get(i), r, eCopy.subtract(sCopy).length());
                if(oldCollisions != null){
                    for(NavLine line : oldCollisions){
                        if(!collisions.contains(line)){
                            return getIntersectingPoint(line, triangles.get(i+1));
                        }
                    }
                }
                oldCollisions = collisions;
            }
            return -1;
        }

        int getIntersectingPoint(NavLine n, NavTriangle t){
            for(int i = 0; i < 3; i++){
                if(n.getP1() == t.getPoints()[i]){
                    return n.getP1();
                }
                if(n.getP2() == t.getPoints()[i]){
                    return n.getP2();
                }
            }
            return -1;
        }

        /**
         * Calculates the collision-lines between a triangle and a ray.
         * @param t the triangle.
         * @param r the ray.
         * @param length length of the ray.
         * @return list of collision-lines.
         */
        private List<NavLine> getCollisionSides(NavTriangle t, Ray r, float length){

            List<NavLine> lineResult = new LinkedList<>();
            Vector3f[] trianglePoints = navigationMesh.getPoints(t);
            Vector3f[] triangleVectors = {trianglePoints[1].subtract(trianglePoints[0]), trianglePoints[2].subtract(trianglePoints[1]), trianglePoints[0].subtract(trianglePoints[2])};

            for(Vector3f p : trianglePoints){
                if(p.equals(r.getOrigin())){
                    return lineResult;
                }
            }

            Vector3f end = r.getOrigin().add(r.getDirection().mult(length));
            Line2D ray = new Line2D.Float(r.getOrigin().getX(), r.getOrigin().getZ(), end.getX(), end.getZ());

            List<Point2D> lastCollisions = new LinkedList<>();
            for(int i = 0; i < 3; i++){
                Line2D tLine = new Line2D.Float(trianglePoints[i].getX(), trianglePoints[i].getZ(), trianglePoints[i].getX() + triangleVectors[i].getX(),
                        trianglePoints[i].getZ() + triangleVectors[i].getZ());
                Point2D collision = intersectLines(ray, tLine);

                if(collision != null){
                    if(!lastCollisions.removeIf(point2D -> collision.equals(point2D))){
                        lastCollisions.add(collision);
                    }
                    lineResult.add(new NavLine(t.getPoints()[i], i < 2 ? t.getPoints()[i+1] : t.getPoints()[0]));
                }
            }
            return lineResult;
        }

    }

    /**
     * Calculates the intersection-point between two lines.
     * @param l the first line.
     * @param m the other line.
     * @return the intersection-point or null if there is no intersection-point.
     */
    public static Point2D.Float intersectLines(Line2D l, Line2D m) {
        // Wegen der Lesbarkeit
        double x1 = l.getX1();
        double x2 = l.getX2();
        double x3 = m.getX1();
        double x4 = m.getX2();
        double y1 = l.getY1();
        double y2 = l.getY2();
        double y3 = m.getY1();
        double y4 = m.getY2();

        double zx = (x1 * y2 - y1 * x2)*(x3-x4) - (x1 - x2) * (x3 * y4 - y3 * x4);
        double zy = (x1 * y2 - y1 * x2)*(y3-y4) - (y1 - y2) * (x3 * y4 - y3 * x4);

        double n = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);

        float x = (float)(zx/n);
        float y = (float)(zy/n);

        if (Float.isNaN(x)& Float.isNaN(y))
        {
            return null;
        }
        if ((x - x1) / (x2 - x1) > 1 || (x - x3) / (x4 - x3) > 1 || (y - y1) / (y2 - y1) > 1 || (y - y3) / (y4 - y3) > 1 )
        {
            return null;
        }
        if((x <= x1 && x <= x2) || (x <= x3 && x <= x4) || (y <= y1 && y <= y2) || (y <= y3 && y <= y4)){
            return null;
        }
        return new Point2D.Float(x,y);
    }

    /**
     * Returns the smallest distance between a triangle and a ray.
     * @param t the triangle.
     * @param r the ray.
     * @return smallest vector between the triangle t and the ray r.
     */
    public static Vector3f getRayDistance(Triangle t, Ray r){
        Vector3f distance = null;
        for(int i = 0; i < 3; i++){
            Vector3f d = t.get(i).subtract(r.origin).cross(r.direction).divide(r.direction);
            if(distance == null || d.length() < distance.length()){
                distance = d;
            }
        }
        return distance;
    }
}
