package pathfinding;

import com.jme3.math.Triangle;
import com.jme3.math.Vector3f;
import com.jme3.scene.Mesh;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by Jakob on 15.01.2015.
 */
public class NavigationMesh {
    private Mesh navMesh;
    private List<NavTriangle> navTriangles;
    private Map<Integer, Vector3f> points;
    private List<Integer> temporaerPointIndexes;
    private boolean initialized = false;

    public NavigationMesh(Mesh mesh){
        this.navMesh = mesh;
    }

  public void initialize(){
      temporaerPointIndexes = new LinkedList<Integer>();
      Map<Vector3f, Integer> pointsReference = new HashMap<>(navMesh.getVertexCount());
      points = new HashMap<>(navMesh.getVertexCount());
      navTriangles = new LinkedList<NavTriangle>();

      int c = navMesh.getTriangleCount();
      for(int i = 0; i < c; i++){
          Triangle t = new Triangle();
          navMesh.getTriangle(i,t);
          NavTriangle nt = new NavTriangle(i);

          if(!points.containsValue(t.get1())){
                points.put(points.size(), t.get1());
              pointsReference.put(t.get1(), points.size()-1);
          }
          if(!points.containsValue(t.get2())){
              points.put(points.size(), t.get2());
              pointsReference.put(t.get2(), points.size()-1);
          }
          if(!points.containsValue(t.get3())){
              points.put(points.size(), t.get3());
              pointsReference.put(t.get3(), points.size()-1);
          }

          nt.setPoints(pointsReference.get(t.get1()), pointsReference.get(t.get2()), pointsReference.get(t.get3()));

          navTriangles.add(nt);
      }

      for(int i = 0; i < navTriangles.size(); i++){
          for(int j = 0; j< navTriangles.size(); j++){
              int cnt = 0;
              for(int k = 0; k < 3; k++){
                  for(int l = 0; l < 3; l++) {
                      if (navTriangles.get(i).getPoints()[k].equals(navTriangles.get(j).getPoints()[l])) {
                          cnt++;
                      }
                  }
              }

              if(cnt == 2){
                  navTriangles.get(i).addNeighbour(navTriangles.get(j));
              }
          }
      }

      initialized = true;
  }

    public Vector3f getPoint(int id){
        return points.get(id);
    }

    public List<Vector3f> getPoints(List<Integer> ids){
        List<Vector3f> vectors = new LinkedList<Vector3f>();
        for(Integer i : ids){
            vectors.add(getPoint(i));
        }
        return vectors;
    }

    public int addPointTemporarily(Vector3f point){
        points.put(points.size(), point);
        temporaerPointIndexes.add(points.size()-1);
        return points.size()-1;
    }

    public void removeAllTemporaerPoints(){
        temporaerPointIndexes.forEach(points::remove);
    }

    public NavTriangle getTriangle(Vector3f position){
        if(!initialized){
            throw new UnsupportedOperationException("initialized has to be called before.");
        }

        //get nearest point
        double nearestDistance = 0;
        int id = -1;
        for(int i = 0; i < points.size(); i++){
            if(!temporaerPointIndexes.contains(i) && (id == -1 || points.get(i).subtract(position).length() < nearestDistance)){
                nearestDistance = points.get(i).subtract(position).length();
                id = i;
            }
        }

        //get all Triangles which contain this point
        List<NavTriangle> triangles = new LinkedList<NavTriangle>();
        for(NavTriangle t : navTriangles){
            if(t.containsPoint(id)){
                triangles.add(t);
            }
        }

        //get Triangle where the position is in
        for(NavTriangle t : triangles){
            if(pointInTriangle(position, t)){
                return  t;
            }
        }
        if(triangles.size() > 0) {
            return triangles.get(0);
        }else{
            return null;
        }
    }

    /**
     * Returns true if the given point is in a 2-dimensional triangle
     * @param point
     * @param triangle
     * @return
     */
    public boolean pointInTriangle(Vector3f point, NavTriangle triangle) {
        if(!initialized){
            throw new UnsupportedOperationException("initialized has to be called before.");
        }
        /*var A = 1/2 * (-p1.y * p2.x + p0.y * (-p1.x + p2.x) + p0.x * (p1.y - p2.y) + p1.x * p2.y);
        var sign = A < 0 ? -1 : 1;
        var s = (p0.y * p2.x - p0.x * p2.y + (p2.y - p0.y) * p.x + (p0.x - p2.x) * p.y) * sign;
        var t = (p0.x * p1.y - p0.y * p1.x + (p0.y - p1.y) * p.x + (p1.x - p0.x) * p.y) * sign;

        return s > 0 && t > 0 && (s + t) < 2 * A * sign;*/
        return pointInTriangle(point, getPoints(triangle));
    }

    public static boolean pointInTriangle(Vector3f point, Vector3f[] p){
        double a = 1/2 * (-p[0].getY() * p[2].getX() + p[0].getY() * (-p[1].getX() + p[2].getX()) + p[0].getX() * (p[1].getY() - p[2].getY()) + p[1].getX() + p[2].getY());
        double sign = a < 0 ? -1: 1;
        double s = (p[0].getY() * p[2].getX() - p[0].getX() * p[2].getY() + (p[2].getY() - p[0].getY()) * point.getX() + (p[0].getX() - p[2].getX()) * point.getY()) * sign;
        double t = (p[0].getX() * p[1].getY() - p[0].getY() * p[1].getX() + (p[0].getY() - p[1].getY()) * point.getX() + (p[1].getX() - p[0].getX()) * point.getY()) * sign;

        return !(s > 0 && t > 0 && (s + t) < 2 * a * sign);
    }

    public Vector3f[] getPoints(NavTriangle triangle){
        if(!initialized){
            throw new UnsupportedOperationException("initialized has to be called before.");
        }
        Integer[] pointIds = triangle.getPoints();
        Vector3f[] r = {points.get(pointIds[0]), points.get(pointIds[1]), points.get(pointIds[2])};
        return r;
    }

    public Vector3f getMiddle(NavTriangle triangle){
        if(!initialized){
            throw new UnsupportedOperationException("initialized has to be called before.");
        }
        Integer[] pointIds = triangle.getPoints();
        Vector3f[] p = {points.get(pointIds[0]), points.get(pointIds[1]), points.get(pointIds[2])};
        return NavTriangle.getMiddle(p);
    }

    public double getGValue(PathNode a, PathNode b){
        return getMiddle(b.getTriangle()).subtract(getMiddle(a.getTriangle())).getZ();
    }
}
