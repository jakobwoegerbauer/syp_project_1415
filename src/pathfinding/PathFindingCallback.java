package pathfinding;

import com.jme3.math.Vector3f;

import java.util.List;

/**
 * Created by Jakob on 22.02.2015.
 */
public interface PathFindingCallback {
    public void pathFound(List<Vector3f> points, Object data);
}
