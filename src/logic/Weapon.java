package logic;

import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.time.Instant;
import java.util.LinkedList;
import java.util.List;

public class Weapon extends Tool {

    private Node model;
    private float precision; // [precision] = mil (= 1000m distance, 10cm unprecision => 1 mil)
	private float fireRate;
	private long reloadTime;
	private Magazine magazine;
	private List<BulletType> compatibleBulletTypes;
	private float strength;
    private List<MagazineEmptyListener> magazineEmptyListener;
    private List<NewShotListener> newShotListeners;
    private Instant lastShot;

    /**
     * Creates a new Weapon with the given properties.
     * @param name name of the weapon
     * @param size size (Inventory)
     * @param weight weight (Inventory)
     * @param fireRate shots per second
     * @param reloadTime time to reload in ms
     * @param strength strength
     */
    public Weapon(String name, double size, double weight, float fireRate, long reloadTime, float strength, float precision) {
        super(name, size, weight);
        if(name == null){
            throw new IllegalArgumentException("name must not be null");
        }
        if(this.fireRate < 0){
            throw new IllegalArgumentException("fireRate has to be bigger than 0");
        }
        if(reloadTime < 0){
            throw new IllegalArgumentException("reloadTime has to be bigger than 0");
        }
        if(strength < 0){
            throw new IllegalArgumentException("strength has to be bigger than 0");
        }
        if(precision < 0){
            throw new IllegalArgumentException("precision has to be bigger than 0");
        }

        this.fireRate = fireRate;
        this.reloadTime = reloadTime;
        this.strength = strength;
        this.precision = precision;
        magazineEmptyListener = new LinkedList<MagazineEmptyListener>();
        newShotListeners = new LinkedList<NewShotListener>();
        compatibleBulletTypes = new LinkedList<BulletType>();
        model = new Node();
        lastShot = Instant.now();
    }

    public Weapon clone() {
        throw new NotImplementedException();
    }

    public void setModel(Node model){
        this.model = model;
    }

    /**
     * Shoots the Weapon if there are bullets left and the last shot is out for long enough
     * @param direction
     * @return
     */
    public boolean shoot(Vector3f direction) {
        if(magazine == null || !magazine.canShoot()){
            return false;
        }
        if(Instant.now().minusMillis((long)(1000.0f/fireRate)).isBefore(lastShot)){
            return false;
        }

        direction.normalize().mult(strength/magazine.getBulletType().getWeight());
        Shot s = new Shot(model.getLocalTranslation(), Shot.addDrift(direction, precision), magazine.getBulletType());
        for(NewShotListener l : newShotListeners){
            l.onNewShot(s);
        }

        lastShot = Instant.now();
        magazine.shoot();

        if(magazine.getBulletCount() == 0){
            for(MagazineEmptyListener m : magazineEmptyListener){
                m.handleMagazineEmpty();
            }
        }

        return true;
	}

    public float getFireRate() {
        return fireRate;
    }

    public long getReloadTime() {
        return reloadTime;
    }

    /**
     * Reloads the weapon
     * @param magazine
     * @return
     */
	public boolean reload(logic.Magazine magazine) {
		if(magazine == null){
            throw new IllegalArgumentException("magazine must not be null");
        }
        if(!compatibleBulletTypes.contains(magazine.getBulletType())){
            return false;
        }
        this.magazine = magazine;
        lastShot = Instant.now().plusMillis(reloadTime).minusMillis((long)(1000.0f/fireRate));

        return true;
	}

    /**
     * Adds a compatible BulletType to it's comp. bullettype list.
     * @param bt
     */
    public void addCompatibleBulletType(BulletType bt){
        if(bt == null){
            throw new IllegalArgumentException("bt must not be null");
        }
        compatibleBulletTypes.add(bt);
    }

    /**
     * Adds a magazineEmptyListener to it's list.
     * @param magazineEmptyListener
     */
    public void addMagazineEmptyListener(MagazineEmptyListener magazineEmptyListener){
        if(magazineEmptyListener == null){
            throw new IllegalArgumentException("magazineEmptyListener must not be null");
        }
        this.magazineEmptyListener.add(magazineEmptyListener);
    }

    /**
     * Adds a NewShotListener to it's list.
     * @param newShotListener
     */
    public void addNewShotListener(NewShotListener newShotListener){
        if(newShotListeners != null){
            throw new IllegalArgumentException("newShotListener must not be null");
        }
        this.newShotListeners.add(newShotListener);
    }

    public List<BulletType> getCompatibleBulletTypes() {
        return compatibleBulletTypes;
    }
}
