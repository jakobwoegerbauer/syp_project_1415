package logic;

import java.util.LinkedList;
import java.util.List;

public class Inventory {

	private List<InventoryItem> items;
	private double maxSize;
	private double maxWeight;
	private double size;
	private double weight;

    /**
     * Creates a new Inventory
     * @param maxWeight
     * @param maxSize
     */
    public Inventory(double maxWeight, double maxSize) {
        this.maxWeight = maxWeight;
        this.maxSize = maxSize;
        items = new LinkedList<InventoryItem>();
    }

    /**
     * adds an item to the inventory
     * @param item the item to add
     * @return if the item could be added or not
     */
	public boolean addItem(InventoryItem item) {
		if(item == null){
            throw new NullPointerException("item must not be null!");
        }

        if(!canAdd(item)){
            return false;
        }

        items.add(item);
        this.weight += item.getWeight();
        this.size += item.getSize();

        return true;
	}

    /**
     * Checks, if an item can be added to the inventory
     * @param item item to be checked
     * @return true, if the item can be added
     */
	public boolean canAdd(InventoryItem item) {
        return !(this.weight + item.getWeight() > maxWeight || this.size + item.getSize() > maxSize);
	}

    /**
     * Returns the a copy of the list with all inventoryitems in the inventory.
     * @return
     */
	public List<InventoryItem> getItems() {
		return new LinkedList<InventoryItem>(items);
	}

	public double getSize() {
		return size;
	}

	public double getWeight() {
		return weight;
	}

    /**
     * Counts, how many of a Item are in the Inventory.
     * @param item
     * @return Number of the items
     */
	public int hasItem(InventoryItem item) {
        int cnt = 0;
        for(InventoryItem i : items){
            if(i.equals(item)){
                cnt++;
            }
        }
        return cnt;
    }

    /**
     * Removes the given InventoryItem.
     * @param item
     * @return the InventoryItem
     */
	public InventoryItem removeItem(InventoryItem item) {
        InventoryItem i = null;
		if(items.contains(item)){
            i = items.get(items.indexOf(item));
            weight -= item.getWeight();
            size -= item.getSize();
            items.remove(item);
        }
        return i;
	}

    public Inventory clone(){
        Inventory i =  new Inventory(maxWeight,maxSize);
        for(InventoryItem ii : getItems()){
            i.addItem(ii.clone());
        }
        return i;
    }

    /**
     * Needed for healing infantry units.
     * @return
     */
    public static Tool getFirstAidKit(){
        return new Tool("Medikit",0.6,1.4);
    }

    public static Tool getRepairKit(){
        return new Tool("Repair-Kit",13.5,10);
    }
}
