package logic;

import java.util.List;

/**
 * Created by Jakob on 15.04.2015.
 */
public class LevelConfig {
    public String levelname;
    public String factoryPath;
    public String mapPath;
    public List<Player> players;

    public LevelConfig(String levelname, String factoryPath, String mapPath, List<Player> players) {
        this.levelname = levelname;
        this.factoryPath = factoryPath;
        this.mapPath = mapPath;
        this.players = players;
    }
}
