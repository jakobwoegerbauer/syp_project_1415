package logic;

import com.jme3.math.Vector3f;

import java.util.List;

public class Base {
    private int maxHealth;
	private int health;
	private Inventory inventory;
	private Vector3f position;

    public Base(int maxHealth, Inventory inventory, Vector3f position) {
        // validate args
        if (maxHealth <= 0) {
            throw new IllegalArgumentException("maxHealth must exceed 0");
        } else if (position == null) {
            throw new IllegalArgumentException("position mustn't be null");
        } else  if (inventory == null) {
            throw new IllegalArgumentException("inventory mustn't be null");
        }

        // do stuff
        this.health = this.maxHealth = maxHealth;
        this.inventory = inventory;
        this.position = position;
    }

    public int getHealth() {
        return health;
    }

    public boolean canAddItem(InventoryItem item) {
        return inventory.canAdd(item);
    }
    public boolean addItem(InventoryItem item) {
        return inventory.addItem(item);
    }
    public List<InventoryItem> getItems() {
        return inventory.getItems();
    }
    public double getInventorySize() {
        return inventory.getSize();
    }
    public double getInventoryWeight() {
        return inventory.getWeight();
    }
    public int hasItem(InventoryItem item) {
        return inventory.hasItem(item);
    }
    public InventoryItem removeItem(InventoryItem item) {
        return inventory.removeItem(item);
    }

    public Vector3f getPosition() {
        return position;
    }
}
