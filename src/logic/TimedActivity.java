package logic;

/**
 * Created by simonlammer on 13.02.15.
 */
public class TimedActivity extends Activity {
    private int duration;
    private float elapsedTime;

    public TimedActivity(Action action, int duration) {
        super(action);
        if (duration <= 0) {
            throw new IllegalArgumentException("Duration must exceed 0");
        }
        this.duration = duration;
        this.elapsedTime = 0;
    }

    @Override
    public boolean finished() {
        return elapsedTime == -1;
    }

    public void update(float tpf) {
        if (!finished()) {
            if (elapsedTime + tpf >= duration) { // finish activity
                performAction(duration - elapsedTime);
                elapsedTime = -1;
                activityFinished();
            } else {
                performAction(tpf);
                elapsedTime += tpf;
            }
        }
    }
}
