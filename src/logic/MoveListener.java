package logic;

import com.jme3.math.Vector3f;

public interface MoveListener {

	void handleMoveEvent(Vector3f position);

}
