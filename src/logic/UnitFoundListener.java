package logic;

/**
 * Created by Jakob on 06.05.2015.
 */
public interface UnitFoundListener {
    public void onUnitFound(Unit unit);
}
