package logic;

import com.jme3.math.Vector3f;

public interface Reactable {

	boolean inRadius(Vector3f position);

	public void react();

}
