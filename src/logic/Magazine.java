package logic;

public class Magazine extends Tool {

	private int bulletCount;
	private BulletType bulletType;
	private int maxBullets;

    public Magazine(String name, double size, double weight, int bulletCount, BulletType bulletType, int maxBullets) {
        super(name, size, weight);

        if(bulletType == null){
            throw new IllegalArgumentException("BulletType must not be null");
        }
        if(name == null){
            throw new IllegalArgumentException("name must not be null");
        }
        if(bulletCount > maxBullets){
            throw new IllegalArgumentException("bulletCount must not be bigger than maxBullets");
        }

        this.bulletCount = bulletCount;
        this.bulletType = bulletType;
        this.maxBullets = maxBullets;
    }

    private Magazine(Magazine old) {
        this(old.getName(), old.getSize(), old.getWeight(), old.bulletCount, old.bulletType, old.maxBullets);
    }

    /**
     * Returns, if there is at least one bullet left.
     * @return
     */
    public boolean canShoot(){
        return getBulletCount() > 0;
    }

    public Magazine clone() {
        return new Magazine(this);
    }

    /**
     * Removes a bullet from the magazine. Throws exception if canShoot returns false.
     * @return
     * @throws UnsupportedOperationException
     */
    public boolean shoot() throws UnsupportedOperationException {
        if(!canShoot()){
            throw new UnsupportedOperationException("No bullet left. Check canShoot before shooting.");
        }
        bulletCount = getBulletCount() - 1;
        return true;
    }

    @Override
    public boolean equals(Object o) {
       if(!o.getClass().equals(this.getClass())){
           return false;
       }
        return this.getName().equals(((Magazine)o).getName());
    }

    @Override
    public int hashCode() {
     return this.getName().hashCode();
    }

    public int getBulletCount() {
        return bulletCount;
    }

    public BulletType getBulletType() {
        return bulletType;
    }

    public int getMaxBullets() {
        return maxBullets;
    }
}
