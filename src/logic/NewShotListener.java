package logic;

/**
 * Created by Jakob on 04.02.2015.
 */
public interface NewShotListener {
    public void onNewShot(Shot shot);
}
