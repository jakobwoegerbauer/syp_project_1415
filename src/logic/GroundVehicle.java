package logic;

import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import pathfinding.PathFinder;

import java.util.Iterator;

public class GroundVehicle extends Vehicle {
    private PathFinder pathFinder;

	public GroundVehicle(String name, double speed, double size, double weight, double swimSpeed, int health, double fuel, Inventory inventory, Node model) {
		super(name, speed, size, weight, swimSpeed, health, fuel, inventory, model);
	}

	public void update(float tpf) {
		super.update(tpf);
	}

	public void driveTo(Vector3f position) {
        // already moving?
        Iterator<Activity> iterator = getActivities().iterator();
        while (iterator.hasNext()) {
            Activity item = iterator.next();
            if (item.getAction().getClass().equals(Move.class)) {
                iterator.remove();
                break;
            }
        }

        // new action
        Move action = new Move(position, this, pathFinder);
        ManualActivity activity = new ManualActivity(action);
        action.setActivity(activity);
        super.addActivity(activity);
	}

    @Override
    public Vector3f getPosition() {
        return super.getPosition();
    }

    public void setPathFinder(PathFinder pathFinder) {
        this.pathFinder = pathFinder;
    }
}
