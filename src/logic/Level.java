package logic;

import com.jme3.asset.AssetManager;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import org.xml.sax.SAXException;
import pathfinding.PathFinder;
import xml.XMLLoader;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

// TODO implement Level
public class Level implements MoveListener, NewShotListener, OnImpactListener {

    private String levelName;
    private String description;
	private Map map;
	private Node node;
	private List<Player> players;
	private List<Shot> shots;
	private ToolFactory toolFactory;
	private VehicleFactory vehicleFactory;
	private WeaponFactory weaponFactory;
    private MagazineFactory magazineFactory;
    private PathFinder pathFinder;
    private AssetManager assetManager;
    private List<Unit> units;

    public Level(String name, String description, Player[] players, Map map, ToolFactory toolFactory, MagazineFactory magazineFactory, WeaponFactory weaponFactory, VehicleFactory vehicleFactory) {
        node = new Node();
        this.levelName = name;
        this.description = description;
        this.players = Arrays.asList(players);
        this.map = map;
        this.toolFactory = toolFactory;
        this.magazineFactory = magazineFactory;
        this.weaponFactory = weaponFactory;
        this.vehicleFactory = vehicleFactory;
        this.shots = new LinkedList<>();

        this.node.attachChild(this.map.getNode());
        this.weaponFactory.setLevel(this);
        pathFinder = new PathFinder();
        pathFinder.initialize(map.getNavMesh());

        units = new LinkedList<>();
        for (Player p : players) {
            units.addAll(p.getInfantries());
            units.addAll(p.getVehicles());
            node.attachChild(p.getNode());
            p.setPathFinder(pathFinder);
        }

        for(Unit u : units){
            u.setRootNode(node);
            u.setMap(map);
        }
    }

	public void handleMoveEvent(Vector3f position) {
		// TODO check if there is something to do
	}

    public static Level load(String xmlPath, AssetManager assetManager) {
        Level l = null;
        ParseManager.setAssetManager(assetManager);
        Infantry.init();

        try {
            l = XMLLoader.loadLevelXML(xmlPath, ParseManager.getToolLoader(), ParseManager.getVehicleLoader(), ParseManager.getLevelCreator(), ParseManager.getPlayerCreator(), ParseManager.getUnitGroupCreator(), ParseManager.getFactoryXMLConsumer(), ParseManager.getMapXMLConsumer(), ParseManager.getInfantryStateLoader());
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return l;
    }

    public List<Unit> getUnits() {
        return units;
    }

	public void unload() {
        map = null;
        players = null;
        pathFinder = null;
        toolFactory = null;
        vehicleFactory = null;
        weaponFactory = null;
        magazineFactory = null;
        node.getChildren().clear();
        node = null;
	}

    public void update(float tpf) {
        for (Player p : players) {
            p.update(tpf, map.getNode());
        }
        for (Shot s : shots) {
            s.update(tpf, getUnits(), map.getNode(), map.getMin(), map.getMax());
        }
    }

    @Override
    public void onNewShot(Shot shot) {
        shot.addOnImpactListener(this);
        shots.add(shot);
    }

    @Override
    public void onImpact(Shot shot, Unit unit) {
        if(unit != null){
            unit.hurt(shot.getBulletType().getDamage()*shot.getDirection().length());
        }
        shots.remove(shot);
    }

    public List<Player> getPlayers() {
        return players;
    }

    public String getLevelName() {
        return levelName;
    }

    public String getDescription() {
        return description;
    }

    public Node getNode() {
        return node;
    }
}
