package logic;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;

import java.util.LinkedList;
import java.util.List;

public abstract class Unit implements InventoryItem, Positionable {
    private Vector3f targetPosition;
    private Vector3f actualPosition;
    private float distanceToTarget;

	private Weapon activeWeapon;
	protected double swimSpeed;
	protected int health;
	protected Inventory inventory;
	protected Node model;
	private List<MoveListener> moveListeners;
	private String name;
	protected double speed;
	private UnitGroup unitGroup;
	protected List<Activity> activities;
    private double size;
    private double weight;
    private Map map;
    private int id;
    private Node rootNode;

    public Unit(String name, double speed, double size, double weight, double swimSpeed, int health, Inventory inventory, Node model) {
        this.id = UnitIdHandler.getNextId();

        this.unitGroup = new UnitGroup();
        this.moveListeners = new LinkedList<MoveListener>();
        this.activities = new LinkedList<Activity>();

        this.name = name;
        this.speed = speed;
        this.swimSpeed = swimSpeed;
        this.health = health;
        this.inventory = inventory;
        this.model = model;
        this.setSize(size);
        this.setWeight(weight);
        this.setModelDirection(0);
        this.actualPosition = model.getLocalTranslation();
        this.targetPosition = actualPosition;

        if(activeWeapon == null) {
            for (InventoryItem i : inventory.getItems()) {
                if(i instanceof Weapon){
                    setActiveWeapon((Weapon)inventory.removeItem(i));
                }
            }
        }
    }

    public void setRootNode(Node node){
        this.rootNode = node;
    }

    public Vector3f getPosition(){
        return actualPosition;
    }

    public void setInstantPosition(Vector3f position){
        this.model.setLocalTranslation(position);
        this.model.updateGeometricState();
        this.model.updateModelBound();
    }

    public float getSpeed() {
        return (float)speed;
    }

    public double getSwimSpeed() {
        return swimSpeed;
    }

    public void setPosition(Vector3f position, float distance){
        this.targetPosition = position;
        this.distanceToTarget = distance;
    }

    private void setModelDirection(float direction){
        this.model.setLocalRotation(getRotationQuaternion(direction));
        this.model.updateGeometricState();
        this.model.updateModelBound();
    }

    public static Quaternion getRotationQuaternion(float alpha){
        Quaternion q = new Quaternion();
        q.fromAngleAxis(alpha, Vector3f.UNIT_Y);
        return q;
    }

    protected final boolean addItem(InventoryItem item, Inventory inventory) {
        if (canAddItem(item, inventory)) {
            inventory.addItem(item);
            if(activeWeapon == null && item instanceof Weapon){
                setActiveWeapon((Weapon)item);
            }
            return true;
        } else {
            return false;
        }
    }

    public boolean addItem(InventoryItem item) {
        return addItem(item, inventory);
    }

    public void addMoveListener(MoveListener listener) {
        if (moveListeners == null) {
            moveListeners.add(listener);
        }
    }

    public boolean canAddItem(InventoryItem item) { // TODO: this method should rather call canAddItem with item and inventory!
        return this.inventory.canAdd(item);
    }

    protected final boolean canAddItem(InventoryItem item, Inventory inventory) {
        if (inventory.canAdd(item)) {
            return true;
        }
        return false;
    }

    protected final List<InventoryItem> getItems(Inventory inventory) {
        return inventory.getItems();
    }

    public List<InventoryItem> getItems() { // TODO: this method should rather call getItems with inventory!
        return this.inventory.getItems();
    }

    public String getName() {
        return this.name;
    }

    public double getSize() {
        return this.size;
    }

    public UnitGroup getUnitGroup() {
        return this.unitGroup;
    }

    public double getWeight() {
        return this.weight;
    }

    public int hasItem(InventoryItem item) {
        return hasItem(item, inventory);
    }

    protected final int hasItem(InventoryItem item, Inventory inventory) {
        if(item == null || inventory == null){
            throw new RuntimeException("Item or inventory is null");}

        int count = 0;
        for (InventoryItem search : inventory.getItems()) {
            if (search.equals(item))
                count++;
        }

        return count;
    }

    public void joinUnitGroup(UnitGroup group) {
        if(unitGroup != null) {
            group.addUnit(this);
            unitGroup = group;
        }
    }

    public void leaveUnitGroup() {
        if(unitGroup != null) {
            unitGroup.removeUnit(this);
            unitGroup = null;
        }
    }

    protected final boolean removeItem(InventoryItem item, int count, Inventory inventory) {
        if (this.hasItem(item, inventory) == count) {
            for (int i = 0; i < count; i++)
                inventory.removeItem(item);

            return true;
        } else
            return false;
    }

    public boolean removeItem(InventoryItem item, int count) {
        return removeItem(item, count, inventory);
    }

    public boolean removeMoveListener(MoveListener listener) {
        return moveListeners.remove(listener);
    }

    public void setUnitGroupName(String name) {
        if (unitGroup != null) {
            unitGroup.setName(name);
        }
    }

    public void update(float tpf) {
        refreshPosition(tpf);
        for (Activity a : getActivities()) {
            a.update(tpf);
            if(a.finished()){
                System.out.println("rem");
                activities.remove(a);
            }
        }
    }

    private void refreshPosition(float tpf){
        Vector3f dif = targetPosition.subtract(getPosition());
        if(dif.length() > distanceToTarget) {
            float dir = new Vector2f(dif.getX(), dif.getZ()).normalize().angleBetween(new Vector2f(0, 1));
            setModelDirection(dir);
            float moveDistance = getSpeed() * tpf;
            if (dif.length() + distanceToTarget > moveDistance) {
                setInstantPosition(getPosition().add(dif.normalize().mult(moveDistance)));
            } else {
                setInstantPosition(targetPosition);
            }
        }
    }

    public Node getModel() {
        return model;
    }
    public void setSize(double size) {
        this.size = size;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public List<Activity> getActivities() {
        return new LinkedList<Activity>(activities);
    }

    public void addActivity(Activity activity) {
        this.activities.add(activity);
    }

    public boolean isAlive() {
        return health <= 0;
    }

    public Weapon getActiveWeapon(){
        return activeWeapon;
    }

    /**
     * sets the given weapon as active weapon and returns the former activeWeapon. (Can be null)
     * @param activeWeapon
     * @return
     */
    public Weapon setActiveWeapon(Weapon activeWeapon) {
        Weapon old = this.activeWeapon;
        this.activeWeapon = activeWeapon;
        return old;
    }

    public void hurt(float healthpoints) {
        this.health -= healthpoints;
        if(this.health <= 0){
            this.health = 0;
            activities.clear();
        }
    }

    public abstract Unit clone();

    public void setMap(Map map) {
        this.map = map;
    }

    public int getId() {
        return id;
    }
}
