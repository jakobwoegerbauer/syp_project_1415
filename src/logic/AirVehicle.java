package logic;

import com.jme3.math.Vector3f;
import com.jme3.scene.Node;

public class AirVehicle extends Vehicle {
    private ManualActivity activity;

	public AirVehicle(String name, double speed, double size, double weight, double swimSpeed, int health, double fuel, Inventory inventory, Node model) {
		super(name, speed, size, weight, swimSpeed, health, fuel, inventory, model);
	}

    public void update(float tpf) {
        super.update(tpf);
    }

	public void driveTo(Vector3f position) {
        if (super.getDriver() == null) { // no driver
            throw new NullPointerException("You must set the driver, to drive!");
        } else { // create actions
            Flight flight = new Flight(position);
            ManualActivity activity = new ManualActivity(flight);
            flight.setActivity(activity);
            this.activity = activity;
        }
	}

    private class Flight implements Action {
        // TODO: adjust flightHeight
        private static final float flightHeight = 150; // height at which the AirVehicle flies
        private Vector3f destination;
        private ManualActivity parentActivity;
        private Activity activity;

        public Flight(Vector3f destination) {
            this.destination = destination;

            // moves
            Vector3f position = getPosition();
            Move moveUp = new Move(new Vector3f(position.getX(), flightHeight, position.getZ()), AirVehicle.this, null); // TODO: getLocalTranslation vs getWorldTranslation?
            Move fly = new Move(destination.setY(flightHeight), AirVehicle.this, null);
            Move land = new Move(destination, AirVehicle.this, null);

            ManualActivity activity = new ManualActivity(moveUp);
            moveUp.setActivity(activity);
            activity.addActivityFinishedListener(new ActivityFinishedListener() {
                @Override
                public void handleFinishedActivity() {
                    ManualActivity activity = new ManualActivity(fly);
                    fly.setActivity(activity);
                    activity.addActivityFinishedListener(new ActivityFinishedListener() {
                        @Override
                        public void handleFinishedActivity() {
                            ManualActivity activity = new ManualActivity(land);
                            land.setActivity(activity);
                            activity.addActivityFinishedListener(new ActivityFinishedListener() {
                                @Override
                                public void handleFinishedActivity() {
                                    Flight.this.parentActivity.setFinished();
                                }
                            });
                            Flight.this.activity = activity;
                        }
                    });
                    Flight.this.activity = activity;
                }
            });
            this.activity = activity;

            // tell the activity it is finished
            parentActivity.setFinished();
        }

        @Override
        public void performAction(float tpf) {
            if (parentActivity == null) {
                throw new NullPointerException("You must call setActivity before performing an action!");
            }
            activity.performAction(tpf);
        }

        public void setActivity(ManualActivity activity) {
            this.parentActivity = activity;
        }
    }
}
