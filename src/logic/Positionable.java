package logic;

import com.jme3.math.Vector3f;

/**
 * Created by Jakob on 08.02.2015.
 */
public interface Positionable {
    public Vector3f getPosition();
}
