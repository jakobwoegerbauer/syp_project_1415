package logic;

import javax.naming.OperationNotSupportedException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WeaponFactory {
	private Map<String, Weapon> weapons;
    private Level level;

    public WeaponFactory(List<Weapon> weapons) {
        this.weapons = new HashMap<>();
        for (Weapon m : weapons) {
            this.weapons.put(m.getName(), m);
        }
    }

    public void setLevel(Level level){
        this.level = level;
    }

    public Weapon createWeapons(String name) throws OperationNotSupportedException {
        if(level == null){
            throw new OperationNotSupportedException("Call weaponFactory.setLevel before creating weapons.");
        }
        Weapon w = weapons.get(name).clone();
        w.addNewShotListener(level);
        return w;
    }
}