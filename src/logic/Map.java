package logic;

import com.jme3.math.Vector3f;
import com.jme3.scene.Mesh;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.terrain.Terrain;

// TODO implement map
public class Map {

    private Terrain terrain;
	private String xmlPath;
	private Node mapObjects;
    private Mesh navMesh;
    private Vector3f min;
    private Vector3f max;

    public Map(Terrain terrain, Mesh mesh, Node node) {
        this.terrain = terrain;
        this.navMesh = mesh;
        mapObjects = new Node();
        mapObjects.attachChild(node);
        mapObjects.attachChild((Spatial)terrain);
        min = new Vector3f(-10,-10,-10);
        max = ((Spatial) terrain).getWorldScale();
    }

    public Node getNode() {
        return mapObjects;
    }

    public Mesh getNavMesh() {
        return navMesh;
    }

    public Terrain getTerrain() {
        return terrain;
    }

    public Vector3f getMin() {
        return min;
    }

    public Vector3f getMax() {
        return max;
    }
}
