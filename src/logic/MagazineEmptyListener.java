package logic;

public interface MagazineEmptyListener {

	void handleMagazineEmpty();

}
