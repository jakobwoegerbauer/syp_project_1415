package logic;

import com.jme3.collision.CollisionResults;
import com.jme3.math.Ray;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import javafx.scene.shape.MoveTo;
import pathfinding.PathFinder;
import pathfinding.PathFindingCallback;

import java.util.LinkedList;
import java.util.List;

public class Infantry extends Unit implements PathFindingCallback {

    // Constants
    private static final float EYESHEIGHT = 1.6f;
    public static final double DEFAULTSPEED = 3;
    public static final double DEFAULTSIZE = 100;
    public static final double DEFAULTWEIGHT = 80;
    public static final double DEFAULTSWIMSPEED = 0.5f;
    public static final String[] names = {
            "Franz",
            "ölkj"
    };
    private static int nameIndex = 0;

    /**
     * needed that the class Infantry gets loaded
     */
    public static void init(){

    }

    // static methods
    public static String getNextName() {
        if(nameIndex >= names.length-1){
            nameIndex = 0;
        }
        return names[nameIndex++];
    }

    public static Inventory getDefaultInventory() {
        return new Inventory(20, 25);
    }

    public static Node infantryModel;

    private final int durationHeal = 2000; // [durationHeal] = ms
    private final int durationRepair = 2000; // [durationHeal] = ms
    private Inventory backpack;
    private PathFinder pathFinder;
    private Vehicle isInVehicle;
    private boolean searchingForEnemies;
    private Unit assignedEnemy;
    private boolean magazineEmpty;
    private double precision;

    public Infantry(String name, double speed, double size, double weight, double swimSpeed, int health, Inventory inventory, Node model) {
		super(name, speed, size, weight, swimSpeed, health, inventory, model);
        isInVehicle = null;
        if(getActiveWeapon() != null) {
            getActiveWeapon().addMagazineEmptyListener(() -> magazineEmpty = true);
        }
	}

    public void setBackpack(Inventory backpack) {
        this.backpack = backpack;
    }

	public boolean addBackpackItem(InventoryItem item) {
        return addItem(item, inventory);
    }

    public boolean canAddBackpackItem(InventoryItem item) {
        return canAddItem(item, backpack);
    }

	public boolean canHeal(Infantry infantry) {
		if(hasItem(Inventory.getFirstAidKit()) >= 1){
            return true;
        }
        return (backpack != null && hasItem(Inventory.getFirstAidKit(),backpack) >= 1);
 	}

    public boolean canRepair(Vehicle vehicle) {
        if (hasItem(Inventory.getRepairKit()) >= 1) {
            return true;
        }
        return (backpack != null && hasItem(Inventory.getRepairKit(), backpack) >= 1);
    }

    public void driveTo(Vehicle vehicle, Vector3f pos) {
        if(vehicle.getDriver() != null && vehicle.getDriver().equals(this)){
            vehicle.driveTo(pos);
            isInVehicle = vehicle;
            return;
        }
        if(isInVehicle != null){
            isInVehicle.releaseDriver();
        }
        addMoveActivity(vehicle, pathFinder, null, MoveAction.DISTANCE_POS_REACHED, false).addActivityFinishedListener(() -> {
            vehicle.setDriver(Infantry.this);
            isInVehicle = vehicle;
            vehicle.driveTo(pos);
            model.setCullHint(Spatial.CullHint.Always);
        });
    }

    public List<InventoryItem> getBackpackItems() {
        return getItems(backpack);
    }

    public List<InventoryItem> getInventoryItems() {
        return getItems(inventory);
    }

    /**
     * Removes all equal items from the inventory and returns them.
     *
     * @param item
     * @param inventory
     * @return
     */
    private static List<InventoryItem> takeItems(InventoryItem item, Inventory inventory) {
        if (inventory == null) {
            throw new IllegalArgumentException("inventory must not be null.");
        }
        List<InventoryItem> items = inventory.getItems();
        List<InventoryItem> result = new LinkedList<>();
        for (InventoryItem i : items) {
            if (i.equals(item)) {
                result.add(i);
                inventory.removeItem(i);
            }
        }
        return result;
    }

    /**
     * Removes all equal items from the inventory and the backpack and returns them.
     *
     * @param item
     * @return
     */
    public List<InventoryItem> takeItems(InventoryItem item) {
        List<InventoryItem> result = takeItems(item, inventory);
        if (backpack != null) {
            result.addAll(takeItems(item, backpack));
        }
        return result;
    }

    /**
     * Removes all equal items from the inventory and returns them.
     *
     * @param item
     * @return
     */
    public List<InventoryItem> takeInventoryItems(InventoryItem item) {
        return takeItems(item, inventory);
    }

    /**
     * @param item
     * @return
     */
    public List<InventoryItem> takeBackpackItems(InventoryItem item) {
        return takeItems(item, backpack);
    }

    /**
     * Removes one equal item from the inventory or the backpack and returns it.
     *
     * @param item
     * @return
     */
    public InventoryItem takeItem(InventoryItem item) {
        InventoryItem i = null;
        if (backpack != null) {
            i = takeBackpackItem(item);
        }
        if (i == null) {
            i = takeInventoryItem(item);
        }
        return i;
    }

    /**
     * Removes one equal item from the inventory and returns it.
     *
     * @param item
     * @return
     */
    public InventoryItem takeInventoryItem(InventoryItem item) {
        List<InventoryItem> items = takeInventoryItems(item);
        if (items.size() >= 1) {
            return items.get(0);
        }
        return null;
    }

    /**
     * Removes one equal item from the backpack and returns it.
     *
     * @param item
     * @return
     */
    public InventoryItem takeBackpackItem(InventoryItem item) {
        List<InventoryItem> items = takeBackpackItems(item);
        if (items.size() >= 1) {
            return items.get(0);
        }
        return null;
    }


    public List<InventoryItem> getItems() {
        List<InventoryItem> items = getInventoryItems();
        if (backpack != null) {
            items.addAll(getBackpackItems());
        }
        return items;
    }

    public int hasBackpackItem(InventoryItem item) {
        return hasItem(item, backpack);
    }

    public int hasInventoryItem(InventoryItem item) {
        return hasItem(item, inventory);
    }

    public int hasItem(InventoryItem item) {
        int i = hasInventoryItem(item);
        if (backpack != null) {
            i += hasBackpackItem(item);
        }
        return i;
    }

    public void heal(Infantry infantry) {
        if(isInVehicle != null){
            isInVehicle.releaseDriver();
        }
        if (!canHeal(infantry)) {
            throw new UnsupportedOperationException("Check canHeal before calling heal!");
        }

        final Infantry i = infantry;
        TimedActivity activity = new TimedActivity(new Action() {
            @Override
            public void performAction(float tpf) {
                i.health += tpf / durationHeal;
            }

        }, (int) ((1.0 - infantry.health) * durationHeal));
        addMoveActivity(infantry, pathFinder, activity, MoveAction.DISTANCE_POS_REACHED, false);
    }

    public Activity moveTo(Vector3f pos) {
        return addMoveActivity(() -> pos, pathFinder, null, MoveAction.DISTANCE_POS_REACHED, false);
    }

    public boolean removeBackpackItem(InventoryItem item, int count) {
        return removeItem(item, 1, backpack);
    }

    public void repair(Vehicle vehicle) {
        if (!canRepair(vehicle)) {
            throw new UnsupportedOperationException("Check canRepair before calling repair!");
        }
        final Vehicle v = vehicle;
        TimedActivity activity = new TimedActivity(new Action() {
            @Override
            public void performAction(float tpf) {
                v.health += tpf / durationRepair;
            }
        }, (int) ((1.0 - vehicle.health) * durationRepair));
        addMoveActivity(vehicle, pathFinder, activity, MoveAction.DISTANCE_POS_REACHED, false);
    }


    @Override
    public void pathFound(List<Vector3f> points, Object data) {
        System.out.println("found " + points.get(points.size()-1));
        ((MoveAction) data).setWayPoints(points);
        ((MoveAction) data).waypointsSet = true;
    }

    /**
     *
     * @param targetPos
     * @param pathfinder
     * @param following
     * @param distance when follow = true => distance to target in sec!
     * @param follow
     * @return
     */
    private Activity addMoveActivity(Positionable targetPos, PathFinder pathfinder, Activity following, float distance, boolean follow) {
        Activity move = getMoveActivity();
        if(move != null){
            activities.remove(move);
        }

        MoveAction action = new MoveAction(this, follow ? targetPos : null) {

            Vector3f nextWaypoint = targetPos.getPosition();
            float elapsedTime = 0;



            @Override
            public void performAction(float tpf) {

                if(follow == null){
                    if ((super.getWayPoints() == null || super.getWayPoints().size() == 0) && waypointsSet) {
                        ((ManualActivity)getMoveActivity()).setFinished();
                        return;
                    }
                    if (getPosition().subtract(super.getWayPoints().get(0)).length() < distance) {
                        if (!super.getWayPoints().isEmpty()) {
                            super.getWayPoints().remove(0);
                        }
                    }
                }else{
                    elapsedTime+=tpf;

                    if(elapsedTime > distance){
                        System.out.println("ö" + nextWaypoint);
                        pathfinder.getInfantryPath(getPosition(), nextWaypoint, (Infantry) getUnit(), this);
                        elapsedTime = 0;
                        nextWaypoint = follow.getPosition();
                    }
                    if (!getWayPoints().isEmpty() && getPosition().subtract(super.getWayPoints().get(0)).length() < MoveAction.DISTANCE_POS_REACHED) {
                        if (!super.getWayPoints().isEmpty()) {
                            super.getWayPoints().remove(0);
                        }
                    }
                }

                if(!super.getWayPoints().isEmpty()) {
                    setPosition(super.getWayPoints().get(0), distance);
                }
            }
        };

        List<Vector3f> w = new LinkedList<>();
        w.add(targetPos.getPosition());
        action.setWayPoints(w);

        pathfinder.getInfantryPath(getPosition(), targetPos.getPosition(), this, action);

        ManualActivity activity = new ManualActivity(action);

        if (following != null) {
            activity.addActivityFinishedListener(() -> activities.add(following));
        }else if(follow){
            //activity.addActivityFinishedListener(() -> addMoveActivity(targetPos,pathfinder,null,distance,true));
        }
        activities.add(activity);
        return  activity;
    }

    public boolean isMoving(){
        return getMoveActivity() != null;
    }

    private Activity getMoveActivity(){
        for(Activity a : activities){
            if(a.getAction() instanceof MoveAction){
                return  a;
            }
        }
        return null;
    }

    /**
     * should be called when a shot hits something nearby.
     */
    public void shotNearby(){
        getUnitGroup().setState(InfantryState.COMBAT);
    }

    public void update(float tpf, Node terrain) {
        super.update(tpf);
        updatePrecision(tpf);
        switch (getUnitGroup().getState()){
            case NORMAL:
                // no enemies in sight
                updateNormal(tpf);
                break;
            case COMBAT:
                // enemies nearby
                updateCombat(tpf, terrain);
                break;
        }
    }

    private void updatePrecision(float tpf) {
        if (assignedEnemy != null && getMoveActivity() == null) {
            precision *= tpf / 15.5;
            precision = precision <= 100 ? precision : 100;
        }
    }

    private void updateNormal(float tpf){
        if(isInVehicle != null){
            setInstantPosition(isInVehicle.getPosition());
            return;
        }
    }

    private void updateCombat(float tpf, Node terrain){
        switch (getUnitGroup().getBehaviour()){
            case OFFENSIVE:
                fight(tpf, terrain);
                break;

            case PASSIVE:
                fight(tpf, terrain);
                break;

            case STEALTH:
                // TODO some alert - show red exclamation mark above infantry when enemy in sight?
                break;
        }
    }

    private void fight(float tpf, Node terrain){
        Activity m = getMoveActivity();
        if(m != null){
            activities.remove(m);
        }
        if(assignedEnemy != null && isInSight(assignedEnemy, terrain)){
            if(magazineEmpty){
                reload();
            }
            getActiveWeapon().shoot(Shot.addDrift(assignedEnemy.getPosition().add(0,0,EYESHEIGHT),25-(precision/4)));
            precision *= tpf / 15.5;
        }
    }

    public void setAssignedEnemy(Unit unit){
        this.assignedEnemy = unit;
        precision = 1;
    }

    private void reload(){
        for(BulletType bulletType : getActiveWeapon().getCompatibleBulletTypes()){
            for(InventoryItem i : getItems()){
                if(i.getClass().equals(Magazine.class)){
                    Magazine m = (Magazine)i;
                    if(m.getBulletType().equals(bulletType) && m.getBulletCount() > 0){
                        if(getActiveWeapon().reload(m)){
                            removeItem(m,1);
                            return;
                        }
                    }
                }
            }
        }

        // no magazine to reload
        // try switching to another weapon
        for(InventoryItem i : getItems()){
            if(i.getClass().equals(Weapon.class)){
                Weapon w = (Weapon)i;
                w.addMagazineEmptyListener(new MagazineEmptyListener() {
                    @Override
                    public void handleMagazineEmpty() {
                        magazineEmpty = true;
                    }
                });
                Weapon old = setActiveWeapon(w);
                removeItem(w,1);
                if(canAddItem(old)){
                    addItem(old);
                }
            }
        }
    }

    private boolean isInSight(Unit unit, Node terrainAndObjects){
        Vector3f dif = getPosition().add(0,EYESHEIGHT,0).subtract(unit.getPosition().add(0,EYESHEIGHT,0));
        Ray r = new Ray(getPosition().add(0,EYESHEIGHT,0), dif);
        CollisionResults cr = new CollisionResults();
        r.collideWith(terrainAndObjects, cr);
        if(cr.size() == 0 || cr.getClosestCollision().getDistance() >= dif.length()){
            return true;
        }
        return false;
    }

    /**
     * Searches for enemies in the given sector
     * @param from in radiant from north
     * @param to in radiant from north
     * example: from = 0; to = Math.PI/2
     *      => searches for enemies in sector from north to east
     */
    public void searchForEnemies(double from, double to, List<Unit> allEnemies, Node terrainAndObjects){
        Thread t = new Thread(() -> {
            for(Unit u : allEnemies){
                if(!searchingForEnemies){
                    return;
                }
                if(!u.isAlive()){
                    continue;
                }
                try {
                    Thread.sleep((int)((to-from) * 3000 / allEnemies.size()));
                } catch (InterruptedException e) {
                    return;
                }
                Vector3f dif = u.getPosition().add(0,EYESHEIGHT,0).subtract(Infantry.this.getPosition().add(0, EYESHEIGHT, 0));
                Vector2f v = new Vector2f(dif.getX(), dif.getZ());
                float angle = v.getAngle();
                if(angle >= from && angle <= to) {
                    Ray r = new Ray(getPosition().add(0,EYESHEIGHT,0), dif.normalize());
                    CollisionResults c = new CollisionResults();
                    r.collideWith(terrainAndObjects, c);
                    if(c.getClosestCollision().getDistance() > dif.length()) {
                        getUnitGroup().enemyFound(u);
                    }
                }
            }
            searchingForEnemies = false;
        });
        searchingForEnemies = true;
        t.setDaemon(true);
        t.start();
    }

    public float getSpeed(){
        return (float)(searchingForEnemies ? 5 * speed / 6 : speed);
    }

    public void follow(Positionable toFollow, float distance) {
        addMoveActivity(toFollow, pathFinder, null, distance, true);
    }

    public Unit getAssignedEnemy() {
        return assignedEnemy;
    }

    @Override
    public Unit clone() {
        return new Infantry(Infantry.getNextName(), speed, getSize(), getWeight(), swimSpeed, health, inventory.clone(), model.clone(true));
    }

    public Vehicle getVehicle()
    {
        return isInVehicle;
    }

    public void setPathFinder(PathFinder pathFinder) {
        this.pathFinder = pathFinder;
    }
}
