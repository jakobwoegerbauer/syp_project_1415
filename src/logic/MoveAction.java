package logic;

import com.jme3.math.Vector3f;

import java.util.List;

/**
 * Created by Jakob on 22.02.2015.
 */
public abstract class MoveAction implements Action{
    public static float DISTANCE_POS_REACHED = 1.0f;
    private List<Vector3f> wayPoints;
    boolean waypointsSet = false;
    Positionable follow;

    public Unit getUnit() {
        return unit;
    }

    private Unit unit;

    public MoveAction(Unit unit, Positionable follow){
        super();
        this.unit = unit;
        this.follow = follow;
    }

    public List<Vector3f> getWayPoints(){
        return this.wayPoints;
    }

    public void setWayPoints(List<Vector3f> wayPoints){
        this.wayPoints = wayPoints;
    }
}
