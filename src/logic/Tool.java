package logic;

public class Tool implements InventoryItem {

	private String name;
	private double size; // [size] dm³
    private double weight; // [weight]= kg


    public Tool(String name, double size, double weight) {
        if(name == null){
            throw new IllegalArgumentException("name must not be null");
        }
        if(size < 0){
            throw new IllegalArgumentException("size has to be >= 0");
        }
        if(weight < 0){
            throw new IllegalArgumentException("weight has to be >= 0");
        }
        this.name = name;
        this.size = size;
        this.weight = weight;
    }

    private Tool(Tool old) {
        this(old.name, old.size, old.weight);
    }

    public Tool clone() {
        return new Tool(this);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public double getSize() {
        return size;
    }

    @Override
    public double getWeight() {
        return weight;
    }

    public boolean equals(Object obj) {
        if(!obj.getClass().equals(this.getClass())){
            return false;
        }
        return ((Tool)obj).getName().equals(this.getName());
	}
	public int hashCode() {
		return this.getName().hashCode();
	}

}
