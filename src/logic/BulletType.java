package logic;

public class BulletType {
	private float weight;
	private float damage;
    private float caliber;
    private float precision;
    private String name;

    public BulletType(String name, float weight, float damage, float caliber, float precision) {
        this.name = name;
        this.weight = weight;
        this.damage = damage;
        this.caliber = caliber;
        this.precision = precision;
    }

    public boolean equals(Object obj) {
        if(!obj.getClass().equals(this.getClass())){
            return false;
        }
        return ((BulletType)obj).getWeight() == getWeight() && ((BulletType)obj).getDamage() == getDamage() && ((BulletType)obj).getCaliber() == getCaliber() &&((BulletType)obj).getPrecision() == getPrecision();
	}

    @Override
    public int hashCode() {
        return Float.hashCode(getDamage());
    }

    public float getWeight() {
        return weight;
    }

    public float getDamage() {
        return damage;
    }

    public float getCaliber() { return caliber; }

    public float getPrecision() {
        return precision;
    }

    public String getName() {
        return name;
    }
}
