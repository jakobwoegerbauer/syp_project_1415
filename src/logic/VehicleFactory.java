package logic;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class VehicleFactory {
	private Map<String, Vehicle> vehicles;

    public VehicleFactory(List<Vehicle> vehicles) {
        this.vehicles = new HashMap<>();
        for (Vehicle m : vehicles) {
            this.vehicles.put(m.getName(), m);
        }
    }

    public Vehicle createVehicles(String name) {
        return vehicles.get(name).clone();
    }
}
