package logic;


public interface Action {

	void performAction(float tpf);
}
