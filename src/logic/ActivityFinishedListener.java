package logic;

public interface ActivityFinishedListener {

	void handleFinishedActivity();

}
