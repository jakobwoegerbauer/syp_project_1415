package logic;

import com.jme3.math.Vector3f;
import pathfinding.PathFinder;
import pathfinding.PathFindingCallback;

import java.util.List;

/**
 * Created by simonlammer on 16.02.15.
 */
public class Move implements Action, PathFindingCallback {
    // TODO: adjust updatePath
    private static final int updatePath = 200; // get new path every x ms
    // TODO: adjust pathNodeDelay
    private static final int pathNodeDelay = 5; // [pathNodeDelay] = ms; time it takes the AirVehicle to move to the next point
    private Vector3f destination;
    private PathFinder pf;
    private Vector3f[] path;
    private int actualPoint;
    private float pathUsage;
    private float moveDelay;
    private ManualActivity parentActivity;
    private Unit unit;
    private PathFinder pathFinder;

    public void generateNewPath() {
        if(pathFinder == null){
            path = new Vector3f[1];
            path[0] = destination;
        }else {
            pathFinder.getInfantryPath(unit.getPosition(), destination, this, null);
        }
        pathUsage = 0;
        actualPoint = 0;
    }

    public Move(Vector3f destination, Unit unit, PathFinder pathFinder) {
        this.unit = unit;
        this.pathFinder = pathFinder;
        this.destination = new Vector3f(destination.getX(), destination.getY(), destination.getZ());
        moveDelay = 0;
    }

    public void setActivity(ManualActivity activity) {
        this.parentActivity = activity;
    }

    @Override
    public void performAction(float tpf) {
        moveDelay += tpf;

        return;
        /*float speed = tpf / pathNodeDelay;


        moveDelay -= pathNodeDelay * speed;
        actualPoint ++;
        if (path != null && actualPoint >= path.length) {
            parentActivity.setFinished();
            return;
        }
        Vector3f p = path[actualPoint];
        unit.setPosition(new Vector3f((float)p.getX(), (float)p.getY(), (float)p.getZ()),MoveAction.DISTANCE_POS_REACHED);
        pathUsage += tpf;
        if (pathUsage >= updatePath) {
            pathUsage = 0;
            generateNewPath();
        }*/
    }

    @Override
    public void pathFound(List<Vector3f> points, Object data) {
        path = (Vector3f[])points.toArray();
        actualPoint = 0;
    }
}
