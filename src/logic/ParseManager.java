package logic;

import com.jme3.asset.AssetManager;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.plugins.blender.BlenderModelLoader;
import com.jme3.terrain.Terrain;
import javafx.util.Pair;
import org.xml.sax.SAXException;
import xml.*;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;

/**
 * Created by Jakob on 06.05.2015.
 */
public class ParseManager {
    private static ToolFactory toolFactory;
    private static MagazineFactory magazineFactory;
    private static WeaponFactory weaponFactory;
    private static VehicleFactory vehicleFactory;
    private static List<BulletType> bulletTypes;
    private static logic.Map map;
    private static AssetManager assetManager;
    private static Node mapObjects;

    // TODO implement Loaders and Creators

    public static void setAssetManager(AssetManager manager){
        assetManager = manager;
        assetManager.registerLoader(BlenderModelLoader.class, "blend");
        bulletTypes = new LinkedList<>();
    }

    // factories

    public static Loader<Node> getNodeLoader(){
        return (String name) -> {
            Node n = new Node();
            n.attachChild(assetManager.loadModel(name));
            return n;
        };
    }

    public static Loader<BulletType> getBulletTypeLoader(){
        return name -> {
            for(BulletType t : bulletTypes){
                if(t.getName().equals(name))
                {
                    return t;
                }
            }
            return null;
        };
    }

    public static BulletTypeCreator getBulletTypeCreator(){
        return (name, caliber, damage, precision, weight) -> new BulletType(name,weight,damage,caliber,precision);
    }

    public static MagazineCreator getMagazineCreator(){
        return (name, size, weight, bulletType, maxBullets) -> new Magazine(name,size,weight,maxBullets,bulletType,maxBullets);
    }

    public static ToolCreator getInventoryItemCreator(){
        return (name, weight, size) -> new Tool(name,size,weight);
    }

    // TODO implement getWeaponCreator (modify XML file)
    public static WeaponCreator getWeaponCreator(){
        return new WeaponCreator() {
            @Override
            public Weapon createWeapon(String name, float weight, float size, float firerate, int reloadtime, BulletType[] bulletTypes, Node model, float strenght, float precision) {
                return new Weapon(name,size,weight,firerate,reloadtime,strenght,precision);
            }
        };
    }

    public static Consumer<BulletType[]> getBulletTypesConsumer(){
        return bulletTypes1 -> {
            bulletTypes.addAll(Arrays.asList(bulletTypes1));
        };
    }

    public static Consumer<MagazineFactory> getMagazineFactoryConsumer(){
        return magazineFactory1 -> magazineFactory = magazineFactory1;
    }

    public static Consumer<ToolFactory> getToolFactoryConsumer(){
        return toolFactory1 -> toolFactory = toolFactory1;
    }

    public static Consumer<VehicleFactory> getVehicleFactoryConsumer(){
        return vehicleFactory1 -> vehicleFactory = vehicleFactory1;
    }

    public static Consumer<WeaponFactory> getWeaponFactoryConsumer(){
        return weaponFactory1 -> weaponFactory = weaponFactory1;
    }

    // Level

    public static Loader<InventoryItem> getToolLoader(){
        return new Loader<InventoryItem>() {
            @Override
            public InventoryItem load(String name) {
                return ParseManager.toolFactory.createTools(name);
            }
        };
    }

    public static Loader<Vehicle> getVehicleLoader(){
        return name -> ParseManager.vehicleFactory.createVehicles(name);
    }

    public static LevelCreator getLevelCreator(){
        return (name, description, factoryXMLFilename, mapFilename, players) -> new Level(name, description, players,map,toolFactory,magazineFactory,weaponFactory,vehicleFactory);
    }

    public static PlayerCreator getPlayerCreator(){
        return (basePosition, inventoryItems, unitGroups) -> {
            List<InventoryItem> items = new LinkedList<>();
            for (Pair<InventoryItem,Integer> p : inventoryItems) {
                for (int i = 0; i < (int) p.getValue(); i++) {
                    items.add(((InventoryItem) p.getKey()).clone());
                }
            }
            return new Player(basePosition, items, unitGroups);
        };
    }

    public static UnitGroupCreator getUnitGroupCreator(){
        return (name, state, infantryCount, vehicles) -> new UnitGroup(name,state,infantryCount,Arrays.asList(vehicles));
    }

    // Map

    public static Loader<Terrain> getTerrainLoader(){
        return new Loader<Terrain>() {
            @Override
            public Terrain load(String name) {
                Spatial map = assetManager.loadModel(name);
                Node n = (Node)map;

                Terrain t = (Terrain)n.getChild("terrain");

                mapObjects = n;
                mapObjects.getChildren().remove(n.getChild("terrain"));

                return t;
            }
        };
    }

    public static Loader<Mesh> getMeshLoader(){
        return name -> {

            Spatial s = assetManager.loadModel(name);

            Mesh m = ((Geometry)((Node)s).getChild(0)).getMesh();
            return m;
        };
    }

    public static MapCreator getMapCreator(){
        return new MapCreator() {
            @Override
            public Map createMap(Terrain terrain, Mesh mesh) {
                return new Map(terrain, mesh, mapObjects);
            }
        };
    }

    // load factories and map

    public static Consumer<String> getFactoryXMLConsumer(){
        return s -> {
            // TODO load factories
            try {
                XMLLoader.loadFactoryXML(s,getNodeLoader(),getBulletTypeLoader(),getBulletTypeCreator(),getMagazineCreator(),getInventoryItemCreator(),
                        getWeaponCreator(),getBulletTypesConsumer(),getMagazineFactoryConsumer(),getToolFactoryConsumer(),getVehicleFactoryConsumer(),
                        getWeaponFactoryConsumer(), getVehicleCreator(), getInfantryNodeConsumer());
            } catch (IOException e) {
                e.printStackTrace();
            } catch (SAXException e) {
                e.printStackTrace();
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            }
        };
    }

    public static Consumer<String> getMapXMLConsumer() {
        return s -> {
            try {
                map = XMLLoader.loadMapXML(s,getTerrainLoader(),getMeshLoader(),getNodeLoader(),getMapCreator());
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            } catch (SAXException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        };
    }

    public static VehicleCreator getVehicleCreator() {
        return (name, maxFuel, type, speed, swimspeed, health, maxInventoryWeight, maxInventorySize, model, size, weight) -> {
            if(type.equalsIgnoreCase("GroundVehicle")){
                return new GroundVehicle(name,speed,size,weight,swimspeed,health,maxFuel,new Inventory(maxInventoryWeight,maxInventorySize),model);
            }
            return new AirVehicle(name,speed,size,weight,swimspeed,health,maxFuel,new Inventory(maxInventoryWeight,maxInventorySize),model);
        };
    }

    public static Loader<InfantryState> getInfantryStateLoader() {
        return name -> {
            if(name.equalsIgnoreCase("combat")){
                return InfantryState.COMBAT;
            }
            return InfantryState.NORMAL;
        };
    }

    public static Consumer<Node> getInfantryNodeConsumer() {
        return node -> Infantry.infantryModel = node;
    }
}