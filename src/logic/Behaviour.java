package logic;

public enum Behaviour {
    OFFENSIVE,
    PASSIVE,
    STEALTH
}
