package logic;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MagazineFactory {
	private Map<String,Magazine> magazines;

    public MagazineFactory(List<Magazine> magazines) {
        this.magazines = new HashMap<String,Magazine>();
        for (Magazine m : magazines) {
            this.magazines.put(m.getName(), m);
        }
    }

	public Magazine createMagazine(String name) {
        return magazines.get(name).clone();
	}
}
