package logic;

/**
 * Created by Jakob on 15.02.2015.
 */
public interface OnImpactListener {
    //unit is null, if the bullet hits a static object
    public void onImpact(Shot shot, Unit unit);
}
