package logic;

import java.util.LinkedList;
import java.util.List;

public abstract class Activity {
    private Action action;
    private List<ActivityFinishedListener> finishedListeners;

    public Activity(Action action) {
        // validate args
        if (action == null) {
            throw new IllegalArgumentException("Action mustn't be null");
        }

        // do stuff 
        this.action = action;
        finishedListeners = new LinkedList<ActivityFinishedListener>();
	}

	public void update(float tpf) {
        action.performAction(tpf);
    }

    public abstract boolean finished();

    protected void activityFinished() {
        callActivityFinishedListeners();
    }

    public void addActivityFinishedListener(ActivityFinishedListener listener) {
        finishedListeners.add(listener);
    }

    private void callActivityFinishedListeners() {
        for (ActivityFinishedListener listener : finishedListeners) {
            listener.handleFinishedActivity();
        }
    }

    protected void performAction(float tpf) {
        action.performAction(tpf);
    }
    
    public boolean removeActivityFinishedListener(ActivityFinishedListener listener) {
        return finishedListeners.remove(listener);
    }

    public Action getAction(){
        return action;
    }
}
