package logic;

import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import pathfinding.PathFinder;

import java.util.LinkedList;
import java.util.List;

public class UnitGroup {
    public static final byte MAX_NAME_LENGTH = 10; // Might change later on
    public static final byte MIN_NAME_LENGTH = 1; // Might change later on
    private InfantryState state;
    private Behaviour behaviour;
	private String name;
	private List<Unit> units;
    private Infantry leader;
    private List<Unit> enemies;
    private List<UnitFoundListener> unitFoundListeners;

    /**
     * Used in public constructor,
     * to only implement name validation once
     */
    public UnitGroup(){
        enemies = new LinkedList<>();
        unitFoundListeners = new LinkedList<>();
    }

    public UnitGroup(String name, InfantryState state, int infantryCount, List<Vehicle> vehicles){
        this();
        this.state = state;
        units = new LinkedList<>();
        setName(name);
        this.behaviour = Behaviour.PASSIVE;

        for (int i = 0; i < infantryCount; i++) {
            Infantry infantry = new Infantry(Infantry.getNextName(), Infantry.DEFAULTSPEED, Infantry.DEFAULTSIZE, Infantry.DEFAULTWEIGHT, Infantry.DEFAULTSWIMSPEED,100,Infantry.getDefaultInventory(),Infantry.infantryModel.clone(true));
            infantry.joinUnitGroup(this);
        }
        for(Vehicle v : vehicles){
            addUnit(v);
        }
    }
    /**
     *
     * @param name new UnitGroup's name
     */
    public UnitGroup(String name) {
        this();
        setName(name);
    }

    public void searchForEnemies(){
        //TODO searchForEnemies
    }

    public void stopSearchingForEnemies(){
        //TODO implement method
    }

    public void addUnit(Unit unit) {
        units.add(unit);
        if(leader == null && unit.getClass().equals(Infantry.class)){
            leader = (Infantry)unit;
        }
    }

    public void setLeader(Infantry unit){
        if(!units.contains(unit)){
            throw new UnsupportedOperationException("a unit has to be in the unitgroup in order to become the leader.");
        }
        leader = unit;
    }

    public Infantry getLeader(){
        return leader;
    }

    public String getName() {
        return name;
    }

    /**
     *
     * @return Clone of every Unit in units as a List<Unit>
     */
    public List<Unit> getUnits() {
        return new LinkedList<Unit>(units);
    }

    public boolean hasUnit(Unit unit) {
        return units.contains(unit);
    }

	public boolean removeUnit(Unit unit) {
        return units.remove(unit);
	}

	public void setName(String name) {
        // valid name?
        if (name == null) {
            throw new IllegalArgumentException("Name mustn't be null");
        } else if (name.length() < MIN_NAME_LENGTH) {
            throw new IllegalArgumentException("Name length must exceed " + MIN_NAME_LENGTH);
        } else if (name.length() > MAX_NAME_LENGTH) {
            throw new IllegalArgumentException("Name length mustn't exceed " + MAX_NAME_LENGTH);
        }

        // set name
        this.name = name;
	}

    public InfantryState getState() {
        return state;
    }

    public void setState(InfantryState state) {
        this.state = state;
    }

    public Behaviour getBehaviour() {
        return behaviour;
    }

    public void moveTo(Vector3f position){

        leader.moveTo(position);

        Thread t = new Thread(() -> {
            Infantry last = leader;
            for(Unit i : units){
                if(!i.equals(leader)){
                    if(i.getClass().equals(Infantry.class)) {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        ((Infantry)i).follow(last, .9f);
                        last = ((Infantry)i);
                    }
                }
            }
        });
        t.setDaemon(true);
        t.start();
    }

    public void reassignEnemies(){
        enemies.stream().filter(u -> u == null || !u.isAlive()).forEach(enemies::remove);
        int enemyIndex = 0;
        for(Unit i : getUnits()){
            if(i.getClass().equals(Infantry.class)) {
                Infantry infantry = (Infantry)i;
                if(infantry.getAssignedEnemy() == null || !infantry.getAssignedEnemy().isAlive()) {
                    infantry.setAssignedEnemy(getEnemies().get(enemyIndex));
                    enemyIndex++;
                    if (enemyIndex >= getEnemies().size()) {
                        enemyIndex = 0;
                    }
                }
            }
        }
        if(enemies.size() > 0){
            setState(InfantryState.COMBAT);
        }else{
            setState(InfantryState.NORMAL);
        }
    }

    public void enemyFound(Unit unit) {
        if(!enemies.contains(unit)){
            enemies.add(unit);
        }
        reassignEnemies();
        for(UnitFoundListener l : unitFoundListeners){
            l.onUnitFound(unit);
        }
    }

    public List<Unit> getEnemies(){
        return new LinkedList<>(enemies);
    }

    public void setPathFinder(PathFinder pathFinder) {
        for(Unit u : units){
            if(u instanceof Infantry){
                ((Infantry)u).setPathFinder(pathFinder);
            }else if(u instanceof GroundVehicle){
                ((GroundVehicle)u).setPathFinder(pathFinder);
            }
        }
    }
}