package logic;

/**
 * Created by Jakob on 11.05.2015.
 */
public class UnitIdHandler {
    private static int lastId;

    public static int getNextId(){
        lastId++;
        return lastId;
    }
}
