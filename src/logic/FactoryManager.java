package logic;

import javax.naming.OperationNotSupportedException;

/**
 * Created by Sir Simon Lammer on 08.05.15.
 */
public class FactoryManager {
    private MagazineFactory magazineFactory;
    private ToolFactory toolFactory;
    private VehicleFactory vehicleFactory;
    private WeaponFactory weaponFactory;

    public FactoryManager(MagazineFactory magazineFactory, ToolFactory toolFactory, VehicleFactory vehicleFactory, WeaponFactory weaponFactory) {
        this.magazineFactory = magazineFactory;
        this.toolFactory = toolFactory;
        this.vehicleFactory = vehicleFactory;
        this.weaponFactory = weaponFactory;
    }

    // TODO If framerate is too low, try whether directly calling the appropriate factory's create method helps - otherwise remove this TODO.
    // CAUTION: Using this function might drop framerate drastically, as many lists get iterated over.
    public Object create(String name) throws OperationNotSupportedException {
        Object res;
        res = magazineFactory.createMagazine(name);
        if (res != null) { return res; }
        res = toolFactory.createTools(name);
        if (res != null) { return res; }
        res = vehicleFactory.createVehicles(name);
        if (res != null) { return res; }
        res = weaponFactory.createWeapons(name);
        return res;
    }
}
