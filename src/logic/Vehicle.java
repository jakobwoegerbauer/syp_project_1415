package logic;

import com.jme3.math.Vector3f;
import com.jme3.scene.Node;

public abstract class Vehicle extends Unit {
	private double fuel;
	private Infantry driver;
	private double maxFuel;

	public Vehicle(String name, double speed, double size, double weight, double swimSpeed, int health, double fuel, Inventory inventory, Node model) {
		//TODO adjust code in order to accept swimSpeed as a double
        super(name, speed, size, weight, (int)swimSpeed, health, inventory, model);
        this.fuel = fuel;
        this.maxFuel = fuel;
	}

    @Override
    public Vehicle clone() {
        if(this instanceof AirVehicle){
            return new AirVehicle(getName(),getSpeed(),getSize(),getWeight(),getSwimSpeed(),health,getFuel(),inventory.clone(),model.clone(true));
        }
        return new GroundVehicle(getName(),getSpeed(),getSize(),getWeight(),getSwimSpeed(),health,getFuel(),inventory.clone(),model.clone(true));
    }

    public abstract void driveTo(Vector3f position);

	public void setDriver(Infantry driver) {
        // validate args
        if (driver == null) {
            throw new IllegalArgumentException("driver mustn't be null");
        }

        // do stuff
        this.driver = driver;
	}

    protected Infantry getDriver() {
        return driver;
    }

    public double getFuel() {
        return fuel;
    }

    public double getMaxFuel(){ return maxFuel; }

    public Infantry releaseDriver() {
        Infantry res = driver;
        driver = null;
        return res;
    }
}
