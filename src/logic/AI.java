package logic;

import com.jme3.math.Vector3f;

import java.util.List;

/**
 * Created by Jakob on 06.05.2015.
 */
public abstract class AI extends Player{

    public AI(Vector3f basePosition, List<InventoryItem> items, List<UnitGroup> unitGroups) {
        super(basePosition, items, unitGroups);
    }

    public abstract void update(float tpf);
}
