package logic;


import java.util.HashMap;
import java.util.List;

public final class ToolFactory {
	private java.util.Map<String, Tool> tools;

    public ToolFactory(List<Tool> tools) {
        this.tools = new HashMap<String, Tool>();
        for (Tool m : tools) {
            this.tools.put(m.getName(), m);
        }
    }

    public Tool createTools(String name) {
        if(!tools.containsKey(name)){
            return null;
        }
        return tools.get(name).clone();
    }

}
