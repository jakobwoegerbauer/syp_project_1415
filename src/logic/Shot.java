package logic;

import com.jme3.collision.CollisionResults;
import com.jme3.math.Ray;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Shot {
    public static Random random = new Random();

    public Vector3f getPosition() {
        return position;
    }

    public Vector3f getDirection() {
        return direction;
    }

    private Vector3f position;
	private Vector3f direction;
	private BulletType bulletType;
    private List<OnImpactListener> onImpactListeners;

    /**
     * Calculates the shot
     * @param tpf time since last update call (in ms).
     * @param units all units on the map.
     * @param node all static objects on the map (incl. terrain).
     * @param min left bottom corner of a box around the map. -|
     * @param max right top corner of a box around the map.----|-->  used to stop the shot, when getting out of bounds.
     */
	public void update(float tpf, List<Unit> units, Node node, Vector3f min, Vector3f max) {

        if(units == null){
            throw new IllegalArgumentException("units must not be null");
        }
        if(node == null){
            throw new IllegalArgumentException("node must not be null");
        }

        direction = direction.mult(0.999999f);
        Vector3f g = new Vector3f(0,-9.81f,0);
        g = g.mult(tpf/1000f);
        direction = direction.add(g);
        Vector3f posNew = position.add(direction.mult(tpf/1000f));
        Ray r = new Ray();
        r.setOrigin(position);
        r.setDirection(direction.normalize());

        List<CollisionData> collisionData = new LinkedList<>();

        // Check collision with units
        CollisionResults c = new CollisionResults();
        for(Unit u : units){
            c = new CollisionResults();
            u.getModel().collideWith(r, c);
            if(c.size() >= 1 && c.getClosestCollision().getContactPoint().distance(r.getOrigin()) <= posNew.subtract(position).length()){
                collisionData.add(new CollisionData(c,u,direction));
                onImpact(c.getClosestCollision().getContactPoint(), direction, u);
            }
        }

        c = new CollisionResults();
        node.collideWith(r, c);
        if(c.size() >= 1 && c.getClosestCollision().getContactPoint().distance(r.getOrigin()) <= posNew.subtract(position).length()){
            collisionData.add(new CollisionData(c,null,direction));
            onImpact(c.getClosestCollision().getContactPoint(), direction, null);
        }

        if(!collisionData.isEmpty()){
            collisionData.sort(new Comparator<CollisionData>() {
                @Override
                public int compare(CollisionData o1, CollisionData o2) {
                    return o1.compareTo(o2);
                }
            });
            CollisionData d = collisionData.get(0);
            onImpact(d.collisionResults.getClosestCollision().getContactPoint(),d.direction,d.unit);
        }


        // Collide with invisible wall to get deleted.
        if(posNew.getX() < min.getX() || posNew.getY() < min.getY() || posNew.getZ() < min.getZ()
                || posNew.getX() > max.getX() || posNew.getY() > max.getY() || posNew.getZ() > max.getZ()){
            onImpact(posNew, direction, null);
        }

        position = posNew;
	}

    private class CollisionData implements Comparable<CollisionData>{
        CollisionResults collisionResults;
        Unit unit;
        Vector3f direction;

        public CollisionData(CollisionResults collisionResults, Unit unit, Vector3f direction) {
            this.collisionResults = collisionResults;
            this.unit = unit;
            this.direction = direction;
        }

        @Override
        public int compareTo(CollisionData o) {
            return Float.compare(collisionResults.getClosestCollision().getDistance(), o.collisionResults.getClosestCollision().getDistance());
        }
    }

    /**
     * Creates a new Shot
     * @param position
     * @param direction
     * @param bulletType
     */
	public Shot(Vector3f position, Vector3f direction, BulletType bulletType) {

        if(position == null){
            throw new IllegalArgumentException("position must not be null.");
        }
        if(direction == null){
            throw new IllegalArgumentException("direction must not be null.");
        }
        if(bulletType == null){
            throw new IllegalArgumentException("bulletType must not be null.");
        }

		this.position = position;
        this.direction = addDrift(direction, bulletType.getPrecision());
        this.bulletType = bulletType;
        this.onImpactListeners = new LinkedList<OnImpactListener>();
	}

    /**
     * Adds a new OnImpactListener to it's list
     * @param listener
     */
    public void addOnImpactListener(OnImpactListener listener){
        onImpactListeners.add(listener);
    }

    /**
     * Is called when the bullet hits an object. Calls onImpact in all OnImpactlisteners in it's list.
     * @param position
     * @param direction
     * @param unit
     */
    private void onImpact(Vector3f position, Vector3f direction, Unit unit){
        for(OnImpactListener l : onImpactListeners){
            l.onImpact(this, unit);
        }
    }

    /**
     * Adds a random inaccuracy to the direction-vector
     * @param origin original vector
     * @param mil max inaccuracy in mil
     * @return the modified vector with same length as origin
     */
    public static Vector3f addDrift(Vector3f origin, double mil){
        float length = origin.length();
        float a = random.nextFloat() * (float)milToRadiant(mil);

        Vector3f rotation = new Vector3f((float)(origin.getX() * Math.cos(a) - origin.getY() * Math.sin(a)),
                (float)(origin.getX() * Math.sin(a) + origin.getY() * Math.cos(a)), origin.getZ());

        origin = Shot.rotate(rotation, origin, random.nextFloat() * (float)Math.PI * 2f);
        return origin.divide(origin.length()).mult(length);
    }

    /**
     * Rotates an vector around an axis in three-dimensional space
     * @param v
     * @param _axis
     * @param ang angle in radiant
     * @return
     */
    public static Vector3f rotate(Vector3f v, Vector3f _axis,float ang)
    {
        float length = v.length();

        v = v.normalize();
        _axis = _axis.normalize();

        Vector3f axis=new Vector3f(_axis.getX(),_axis.getY(),_axis.getZ());
        float _parallel= axis.dot(v);
        Vector3f parallel=axis.mult(_parallel);
        Vector3f perp = parallel.subtract(v);
        Vector3f Cross= v.cross(axis);
        Vector3f result= parallel.add(Cross.mult((float)Math.sin(-ang)).add(perp.mult((float)Math.cos(-ang))));

        result = result.normalize().mult(length);
        return result;
    }

    /**
     * Converts the angle from mil to radiant.
     * @param mil angle in mil
     * @return angle in radiant
     */
    public static double milToRadiant(double mil){
        return mil * (Math.PI/3200.0);
    }

    /**
     * Converts the angle from radiant to mil.
     * @param radiant angle in radiant
     * @return angle in mil
     */
    public static double radiantToMil(double radiant){
        return radiant * (3200.0/Math.PI);
    }

    public BulletType getBulletType() {
        return bulletType;
    }
}
