package logic;

/**
 * Created by simonlammer on 13.02.15.
 */
public class ManualActivity extends Activity {
    private boolean finished = false;

    public ManualActivity(Action action) {
        super(action);
    }

    public void setFinished() {
        if (!finished) {
            finished = true;
            activityFinished();
        }
    }

    public boolean finished() {
        return finished;
    }

    @Override
    public void update(float tpf) {
        if (!finished()) {
            performAction(tpf);
        }
    }
}
