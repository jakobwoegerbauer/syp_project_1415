package logic;


public interface InventoryItem{

	public String getName();

	public double getSize();

	public double getWeight();

	public InventoryItem clone();
}
