package logic;

import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import pathfinding.PathFinder;

import java.util.LinkedList;
import java.util.List;

/**
 *  Schnittstelle User - Logik (nur eigene Einheiten)
 *  beinhaltet Methoden zur Infantry-Steuerung
 */
public class Player {

    private Base base;
    private List<Infantry> infantries;
    private List<Vehicle> vehicles;
    private List<UnitGroup> unitGroups;
    private Node node;

    public List<UnitGroup> getUnitGroups() {
        return new LinkedList<>(unitGroups);
    }

    public LinkedList<Vehicle> getVehicles() {
        return new LinkedList<>(vehicles);
    }

    public List<Infantry> getInfantries() {
        return new LinkedList<Infantry>(infantries);
    }

    public Player(Vector3f basePosition, List<InventoryItem> items, List<UnitGroup> unitGroups) {
        node = new Node();
        base = new Base(100, new Inventory(100000, 100000), basePosition);
        for (InventoryItem i : items) {
            base.addItem(i);
        }
        this.unitGroups = unitGroups;
        this.infantries = new LinkedList<>();
        this.vehicles = new LinkedList<>();

        for (UnitGroup ug : unitGroups) {
            for (Unit u : ug.getUnits()) {
                u.setInstantPosition(basePosition);
                if (u instanceof Infantry) {
                    infantries.add((Infantry) u);
                } else if (u instanceof Vehicle) {
                    vehicles.add((Vehicle) u);
                }
                node.attachChild(u.getModel());
            }
        }
    }

    public void update(float tpf, Node terrain) {
        for (Infantry i : infantries) {
            i.update(tpf, terrain);
        }
        for(Vehicle v : vehicles){
            v.update(tpf);
        }
    }

    public void setPathFinder(PathFinder pathFinder) {
        for (UnitGroup u : unitGroups) {
            u.setPathFinder(pathFinder);
        }
    }

    public Spatial getNode() {
        return node;
    }
}
