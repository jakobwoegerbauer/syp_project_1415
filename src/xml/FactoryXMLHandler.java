package xml;

import com.jme3.scene.Node;
import logic.*;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;

/**
 * Created by Simon Lammer on 30.04.2015.
 */
public class FactoryXMLHandler extends DefaultHandler {

    //region loader
    private Loader<Node> nodeLoader;
    private Loader<BulletType> bulletTypeLoader;
    //endregion
    //region creators
    private BulletTypeCreator bulletTypeCreator;
    private MagazineCreator magazineCreator;
    private ToolCreator toolCreator;
    private WeaponCreator weaponCreator;
    private VehicleCreator vehicleCreator;
    //endregion
    //region tags
    private boolean tag_factories;
    private boolean tag_infantryModelPath;
    private boolean tag_caliber;
    private boolean tag_damage;
    private boolean tag_precision;
    private boolean tag_magazines;
    private boolean tag_magazine;
    private boolean tag_maxBullets;
    private boolean tag_tools;
    private boolean tag_tool;
    private boolean tag_size;
    private boolean tag_weapons;
    private boolean tag_weapon;
    private boolean tag_weight;
    private boolean tag_firerate;
    private boolean tag_reloadTime;
    private boolean tag_bulletTypes;
    private boolean tag_bulletType;
    private boolean tag_vehicles;
    private boolean tag_vehicle;
    private boolean tag_name;
    private boolean tag_maxFuel;
    private boolean tag_type;
    private boolean tag_speed;
    private boolean tag_swimSpeed;
    private boolean tag_health;
    private boolean tag_maxInventoryWeight;
    private boolean tag_maxInventorySize;
    private boolean tag_modelPath;
    private boolean tag_strength;
    //endregion
    //region value storage
    private BulletType bulletType;
    private float caliber;
    private float damage;
    private float precision;
    private float weight;
    private float size;
    private float firerate;
    private float strength;
    private float maxFuel;
    private float speed;
    private float swimSpeed;
    private float maxInventoryWeight;
    private float maxInventorySize;
    private int health;
    private int reloadTime;
    private int maxBullets;
    private List<BulletType> bulletTypes;
    private List<BulletType> possibleBulletTypes;
    private List<Magazine> magazines;
    private List<Tool> tools;
    private List<Weapon> weapons;
    private List<Vehicle> vehicles;
    private Node infantryModel;
    private Node model;
    private String name;
    private String value;
    private String type;
    //endregion
    //region callback
    private Consumer<BulletType[]> bulletTypesConsumer;
    private Consumer<MagazineFactory> magazineFactoryConsumer;
    private Consumer<ToolFactory> toolFactoryConsumer;
    private Consumer<VehicleFactory> vehicleFactoryConsumer;
    private Consumer<WeaponFactory> weaponFactoryConsumer;
    private final Consumer<Node> infantryModelConsumer;
    //endregion

    public FactoryXMLHandler(Loader<Node> nodeLoader, Loader<BulletType> bulletTypeLoader, BulletTypeCreator bulletTypeCreator, MagazineCreator magazineCreator, ToolCreator toolCreator, WeaponCreator weaponCreator, Consumer<BulletType[]> bulletTypesConsumer, Consumer<MagazineFactory> magazineFactoryConsumer, Consumer<ToolFactory> toolFactoryConsumer, Consumer<VehicleFactory> vehicleFactoryConsumer, Consumer<WeaponFactory> weaponFactoryConsumer, VehicleCreator vehicleCreator, Consumer<Node> infantryModelConsumer) {
        this.nodeLoader = nodeLoader;
        this.bulletTypeLoader = bulletTypeLoader;
        this.bulletTypeCreator = bulletTypeCreator;
        this.magazineCreator = magazineCreator;
        this.toolCreator = toolCreator;
        this.weaponCreator = weaponCreator;
        this.bulletTypesConsumer = bulletTypesConsumer;
        this.magazineFactoryConsumer = magazineFactoryConsumer;
        this.toolFactoryConsumer = toolFactoryConsumer;
        this.vehicleFactoryConsumer = vehicleFactoryConsumer;
        this.weaponFactoryConsumer = weaponFactoryConsumer;
        this.vehicleCreator = vehicleCreator;
        this.infantryModelConsumer = infantryModelConsumer;
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        value = new String(ch, start, length);
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        super.endElement(uri, localName, qName);
        boolean err = false;
        if (tag_factories) {
            if (qName.equalsIgnoreCase("factories")) {
                tag_factories = false;
            } else if (tag_infantryModelPath && qName.equalsIgnoreCase("infantrymodelpath")) {
                tag_infantryModelPath = false;
                 infantryModelConsumer.accept(nodeLoader.load(value));
            } else if (tag_bulletTypes && !tag_weapon) {
                if (qName.equalsIgnoreCase("bullettypes")) {
                    tag_bulletTypes = false;
                    bulletTypesConsumer.accept(bulletTypes.toArray(new BulletType[bulletTypes.size()]));
                } else if (tag_bulletType) {
                    if (qName.equalsIgnoreCase("bullettype")) {
                        tag_bulletType = false;
                        bulletTypes.add(bulletTypeCreator.createBulletType(name, caliber, damage, precision, weight));
                    } else if (tag_name && qName.equalsIgnoreCase("name")) {
                        tag_name = false;
                        name = value;
                    } else if (tag_caliber && qName.equalsIgnoreCase("caliber")) {
                        tag_caliber = false;
                        caliber = Float.parseFloat(value);
                    } else if (tag_damage && qName.equalsIgnoreCase("damage")) {
                        tag_damage = false;
                        damage = Float.parseFloat(value);
                    } else if (tag_precision && qName.equalsIgnoreCase("precision")) {
                        tag_precision = false;
                        precision = Float.parseFloat(value);
                    } else if (tag_weight && qName.equalsIgnoreCase("weight")) {
                        tag_weight = false;
                        weight = Float.parseFloat(value);
                    } else {
                        err = true;
                    }
                } else {
                    err = true;
                }
            } else if (tag_magazines) {
                if (qName.equalsIgnoreCase("magazines")) {
                    tag_magazines = false;
                    magazineFactoryConsumer.accept(new MagazineFactory(new LinkedList<>(magazines)));
                } else if (tag_magazine) {
                    if (qName.equalsIgnoreCase("magazine")) {
                        tag_magazine = false;
                        magazines.add(magazineCreator.createMagazine(name, size, weight, bulletType, maxBullets));
                    } else if (tag_name && qName.equalsIgnoreCase("name")) {
                        tag_name = false;
                        name = value;
                    } else if (tag_weight && qName.equalsIgnoreCase("weight")) {
                        tag_weight = false;
                        weight = Float.parseFloat(value);
                    } else if (tag_size && qName.equalsIgnoreCase("size")) {
                        tag_size = false;
                        size = Float.parseFloat(value);
                    } else if (tag_bulletType && qName.equalsIgnoreCase("bullettype")) {
                        tag_bulletType = false;
                        bulletType = bulletTypeLoader.load(value);
                    } else if (tag_maxBullets && qName.equalsIgnoreCase("maxbullets")) {
                        tag_maxBullets = false;
                        maxBullets = Integer.parseInt(value);
                    } else {
                        err = true;
                    }
                } else {
                    err = true;
                }
            } else if (tag_tools) {
                if (qName.equalsIgnoreCase("tools")) {
                    tag_tools = false;
                    toolFactoryConsumer.accept(new ToolFactory(new LinkedList<>(tools)));
                } else if (tag_tool) {
                    if (qName.equalsIgnoreCase("tool")) {
                        tag_tool = false;
                        tools.add(toolCreator.createTool(name, weight, size));
                    } else if (tag_name && qName.equalsIgnoreCase("name")) {
                        tag_name = false;
                        name = value;
                    } else if (tag_weight && qName.equalsIgnoreCase("weight")) {
                        tag_weight = false;
                        weight = Float.parseFloat(value);
                    } else if (tag_size && qName.equalsIgnoreCase("size")) {
                        tag_size = false;
                        size = Float.parseFloat(value);
                    } else {
                        err = true;
                    }
                } else {
                    err = true;
                }
            } else if (tag_weapons) {
                if (qName.equalsIgnoreCase("weapons")) {
                    tag_weapons = false;
                    weaponFactoryConsumer.accept(new WeaponFactory(new LinkedList<>(weapons))); //TODO DO WIAD NULL �BAGEBN, I BRAUCH DO IWIE A LEVEL!!!
                } else if (tag_weapon) {
                    if (qName.equalsIgnoreCase("weapon")) {
                        tag_weapon = false;
                        weapons.add(weaponCreator.createWeapon(name, weight, size, firerate, reloadTime, possibleBulletTypes.toArray(new BulletType[possibleBulletTypes.size()]), model, strength, precision));
                    } else if (tag_name && qName.equalsIgnoreCase("name")) {
                        tag_name = false;
                        name = value;
                    } else if (tag_weight && qName.equalsIgnoreCase("weight")) {
                        tag_weight = false;
                        weight = Float.parseFloat(value);
                    } else if (tag_size && qName.equalsIgnoreCase("size")) {
                        tag_size = false;
                        size = Float.parseFloat(value);
                    } else if (tag_strength && qName.equalsIgnoreCase("strength")) {
                        tag_strength = false;
                        strength = Float.parseFloat(value);
                    } else if (tag_precision && qName.equalsIgnoreCase("precision")) {
                        tag_precision = false;
                        precision = Float.parseFloat(value);
                    } else if (tag_firerate && qName.equalsIgnoreCase("firerate")) {
                        tag_firerate = false;
                        firerate = Float.parseFloat(value);
                    } else if (tag_reloadTime && qName.equalsIgnoreCase("reloadtime")) {
                        tag_reloadTime = false;
                        reloadTime = Integer.parseInt(value);
                    } else if (tag_bulletTypes) {
                        if (qName.equalsIgnoreCase("bullettypes")) {
                            tag_bulletTypes = false;
                        } else if (tag_bulletType && qName.equalsIgnoreCase("bullettype")) {
                            tag_bulletType = false;
                            possibleBulletTypes.add(bulletTypeLoader.load(value));
                        } else {
                            err = true;
                        }
                    } else if (tag_modelPath && qName.equalsIgnoreCase("modelpath")) {
                        tag_modelPath = false;
                        model = nodeLoader.load(value);
                    } else {
                        err = true;
                    }
                } else {
                    err = true;
                }
            } else if (tag_vehicles) {
                if (qName.equalsIgnoreCase("vehicles")) {
                    tag_vehicles = false;
                    vehicleFactoryConsumer.accept(new VehicleFactory(new LinkedList<>(vehicles)));
                } else if (tag_vehicle) {
                    if (qName.equalsIgnoreCase("vehicle")) {
                        tag_vehicle = false;
                        vehicles.add(vehicleCreator.createVehicle(name, maxFuel, type, speed, swimSpeed, health, maxInventoryWeight, maxInventorySize, model,size,weight));
                    } else if (tag_name && qName.equalsIgnoreCase("name")) {
                        tag_name = false;
                        name = value;
                    } else if (tag_maxFuel && qName.equalsIgnoreCase("maxfuel")) {
                        tag_maxFuel = false;
                        maxFuel = Float.parseFloat(value);
                    } else if (tag_type && qName.equalsIgnoreCase("type")) {
                        tag_type = false;
                        type = value;
                    } else if (tag_speed && qName.equalsIgnoreCase("speed")) {
                        tag_speed = false;
                        speed = Float.parseFloat(value);
                    } else if (tag_swimSpeed && qName.equalsIgnoreCase("swimspeed")) {
                        tag_swimSpeed = false;
                        swimSpeed = Float.parseFloat(value);
                    } else if (tag_health && qName.equalsIgnoreCase("health")) {
                        tag_health = false;
                        health = Integer.parseInt(value);
                    } else if (tag_maxInventoryWeight && qName.equalsIgnoreCase("maxinventoryweight")) {
                        tag_maxInventoryWeight = false;
                        maxInventoryWeight = Float.parseFloat(value);
                    } else if (tag_maxInventorySize && qName.equalsIgnoreCase("maxinventorysize")) {
                        tag_maxInventorySize = false;
                        maxInventorySize = Float.parseFloat(value);
                    } else if (tag_modelPath && qName.equalsIgnoreCase("modelpath")) {
                        tag_modelPath = false;
                        model = nodeLoader.load(value);
                    } else if(tag_size && qName.equalsIgnoreCase("size")){
                        tag_size = false;
                        size = Float.parseFloat(value);
                    }else if (tag_weight && qName.equalsIgnoreCase("weight")){
                        tag_weight = false;
                        weight = Float.parseFloat(value);
                    }else{
                        err = true;
                    }
                } else {
                    err = true;
                }
            } else {
                err = true;
            }
        } else {
            err = true;
        }
        if (err) {
            throw new IllegalStateException("Unexpected sequence of tags. Could not close tag '" + qName + "'");
        }
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        super.startElement(uri, localName, qName, attributes);
        boolean err = false;
        if (!tag_factories && qName.equalsIgnoreCase("factories")) {
            tag_factories = true;
        } else if (tag_factories) {
            if (!tag_infantryModelPath && qName.equalsIgnoreCase("infantrymodelpath")) {
                tag_infantryModelPath = true;
            } else if (!tag_bulletTypes && qName.equalsIgnoreCase("bullettypes") && !tag_weapon) {
                tag_bulletTypes = true;
                bulletTypes = new LinkedList<>();
            } else if (tag_bulletTypes && !tag_weapon) {
                if (!tag_bulletType && qName.equalsIgnoreCase("bullettype")) {
                    tag_bulletType = true;
                } else if (tag_bulletType) {
                    if (!tag_name && qName.equalsIgnoreCase("name")) {
                        tag_name = true;
                    } else if (!tag_caliber && qName.equalsIgnoreCase("caliber")) {
                        tag_caliber = true;
                    } else if (!tag_damage && qName.equalsIgnoreCase("damage")) {
                        tag_damage = true;
                    } else if (!tag_precision && qName.equalsIgnoreCase("precision")) {
                        tag_precision = true;
                    } else if (!tag_weight && qName.equalsIgnoreCase("weight")) {
                        tag_weight = true;
                    } else {
                        err = true;
                    }
                } else {
                    err = true;
                }
            } else if (!tag_magazines && qName.equalsIgnoreCase("magazines")) {
                tag_magazines = true;
                magazines = new LinkedList<>();
            } else if (tag_magazines) {
                if (!tag_magazine && qName.equalsIgnoreCase("magazine")) {
                    tag_magazine = true;
                } else if (tag_magazine) {
                    if (!tag_name && qName.equalsIgnoreCase("name")) {
                        tag_name = true;
                    } else if (!tag_weight && qName.equalsIgnoreCase("weight")) {
                        tag_weight = true;
                    } else if (!tag_size && qName.equalsIgnoreCase("size")) {
                        tag_size = true;
                    } else if (!tag_bulletType && qName.equalsIgnoreCase("bullettype")) {
                        tag_bulletType = true;
                    } else if (!tag_maxBullets && qName.equalsIgnoreCase("maxbullets")) {
                        tag_maxBullets = true;
                    } else {
                        err = true;
                    }
                } else {
                    err = true;
                }
            } else if (!tag_tools && qName.equalsIgnoreCase("tools")) {
                tag_tools = true;
                tools = new LinkedList<>();
            } else if (tag_tools) {
                if (!tag_tool && qName.equalsIgnoreCase("tool")) {
                    tag_tool = true;
                } else if (tag_tool) {
                    if (!tag_name && qName.equalsIgnoreCase("name")) {
                        tag_name = true;
                    } else if (!tag_weight && qName.equalsIgnoreCase("weight")) {
                        tag_weight = true;
                    } else if (!tag_size && qName.equalsIgnoreCase("size")) {
                        tag_size = true;
                    } else {
                        err = true;
                    }
                } else {
                    err = true;
                }
            } else if (!tag_weapons && qName.equalsIgnoreCase("weapons")) {
                tag_weapons = true;
                weapons = new LinkedList<>();
            } else if (tag_weapons) {
                if (!tag_weapon && qName.equalsIgnoreCase("weapon")) {
                    tag_weapon = true;
                } else if (tag_weapon) {
                    if (!tag_name && qName.equalsIgnoreCase("name")) {
                        tag_name = true;
                    } else if (!tag_weight && qName.equalsIgnoreCase("weight")) {
                        tag_weight = true;
                    } else if (!tag_size && qName.equalsIgnoreCase("size")) {
                        tag_size = true;
                    } else if (!tag_strength && qName.equalsIgnoreCase("strength")) {
                        tag_strength = true;
                    } else if (!tag_precision && qName.equalsIgnoreCase("precision")) {
                        tag_precision = true;
                    } else if (!tag_firerate && qName.equalsIgnoreCase("firerate")) {
                        tag_firerate = true;
                    } else if (!tag_reloadTime && qName.equalsIgnoreCase("reloadtime")) {
                        tag_reloadTime = true;
                    } else if (!tag_bulletTypes && qName.equalsIgnoreCase("bullettypes")) {
                        tag_bulletTypes = true;
                        possibleBulletTypes = new LinkedList<>();
                    } else if (tag_bulletTypes && (!tag_bulletType && qName.equalsIgnoreCase("bullettype"))) {
                        tag_bulletType = true;
                    } else if (!tag_modelPath && qName.equalsIgnoreCase("modelpath")) {
                        tag_modelPath = true;
                    } else {
                        err = true;
                    }
                } else {
                    err = true;
                }
            } else if (!tag_vehicles && qName.equalsIgnoreCase("vehicles")) {
                tag_vehicles = true;
                vehicles = new LinkedList<>();
            } else if (tag_vehicles) {
                if (!tag_vehicle && qName.equalsIgnoreCase("vehicle")) {
                    tag_vehicle = true;
                } else if (tag_vehicle) {
                    if (!tag_name && qName.equalsIgnoreCase("name")) {
                        tag_name = true;
                    } else if (!tag_maxFuel && qName.equalsIgnoreCase("maxfuel")) {
                        tag_maxFuel = true;
                    } else if (!tag_type && qName.equalsIgnoreCase("type")) {
                        tag_type = true;
                    } else if (!tag_speed && qName.equalsIgnoreCase("speed")) {
                        tag_speed = true;
                    } else if (!tag_swimSpeed && qName.equalsIgnoreCase("swimspeed")) {
                        tag_swimSpeed = true;
                    } else if (!tag_health && qName.equalsIgnoreCase("health")) {
                        tag_health = true;
                    } else if (!tag_maxInventoryWeight && qName.equalsIgnoreCase("maxinventoryweight")) {
                        tag_maxInventoryWeight = true;
                    } else if (!tag_maxInventorySize && qName.equalsIgnoreCase("maxinventorysize")) {
                        tag_maxInventorySize = true;
                    } else if (!tag_modelPath && qName.equalsIgnoreCase("modelpath")) {
                        tag_modelPath = true;
                    } else if(!tag_size && qName.equalsIgnoreCase("size")){
                       tag_size = true;
                    }else if(!tag_weight && qName.equalsIgnoreCase("weight")){
                        tag_weight = true;
                    }else {
                        err = true;
                    }
                } else {
                    err = true;
                }
            } else {
                err = true;
            }
        } else {
            err = true;
        }
        if (err) {
            throw new IllegalStateException("Unexpected sequence of tags. Could not open tag '" + qName + "'");
        }
    }
}
