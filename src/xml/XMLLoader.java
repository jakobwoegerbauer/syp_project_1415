package xml;

import com.jme3.scene.Mesh;
import com.jme3.scene.Node;
import com.jme3.terrain.Terrain;
import logic.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.function.Consumer;

/**
 * Created by Simon Lammer on 03.04.15.
 */
public class XMLLoader {
    public static void loadFactoryXML(String filename, Loader<Node> nodeLoader, Loader<BulletType> bulletTypeLoader, BulletTypeCreator bulletTypeCreator, MagazineCreator magazineCreator, ToolCreator inventoryItemCreator, WeaponCreator weaponCreator, Consumer<BulletType[]> bulletTypesConsumer, Consumer<MagazineFactory> magazineFactoryConsumer, Consumer<ToolFactory> toolFactoryConsumer, Consumer<VehicleFactory> vehicleFactoryConsumer, Consumer<WeaponFactory> weaponFactoryConsumer, VehicleCreator vehicleCreator, Consumer<Node> infantryNodeConsumer) throws IOException, SAXException, ParserConfigurationException {
        SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
        FactoryXMLHandler handler = new FactoryXMLHandler(nodeLoader, bulletTypeLoader, bulletTypeCreator, magazineCreator, inventoryItemCreator, weaponCreator, bulletTypesConsumer, magazineFactoryConsumer, toolFactoryConsumer, vehicleFactoryConsumer, weaponFactoryConsumer, vehicleCreator, infantryNodeConsumer);
        parser.parse(filename, handler);
    }

    public static Level loadLevelXML(String filename, Loader<InventoryItem> inventoryItemLoader, Loader<Vehicle> vehicleLoader, LevelCreator levelCreator, PlayerCreator playerCreator, UnitGroupCreator unitGroupCreator, Consumer<String> factoryXMLConsumer, Consumer<String> mapXMLConsumer, Loader<InfantryState> infantryStateLoader) throws ParserConfigurationException, SAXException, IOException {
        SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
        Holder<Level> holder = new Holder<>();
        Consumer<Level> levelConsumer = level -> holder.setValue(level);
        LevelXMLHandler handler = new LevelXMLHandler(inventoryItemLoader, vehicleLoader, levelCreator, playerCreator, unitGroupCreator, levelConsumer, factoryXMLConsumer, mapXMLConsumer, infantryStateLoader);
        parser.parse(filename, handler);
        return holder.getValue();
    }

    public static Map loadMapXML(String filename, Loader<Terrain> terrainLoader, Loader<Mesh> meshLoader, Loader<Node> objectLoader, MapCreator mapCreator) throws ParserConfigurationException, SAXException, IOException {
        SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
        Holder<Map> holder = new Holder<>();
        MapXMLHandler handler = new MapXMLHandler(terrainLoader, meshLoader, objectLoader, mapCreator, map -> holder.setValue(map));
        parser.parse(filename, handler);
        return holder.getValue();
    }
}