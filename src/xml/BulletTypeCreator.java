package xml;

import logic.BulletType;

/**
 * Created by Simon Lammer on 30.04.2015.
 */
public interface BulletTypeCreator {
    public BulletType createBulletType(String name, float caliber, float damage, float precision, float weight);
}
