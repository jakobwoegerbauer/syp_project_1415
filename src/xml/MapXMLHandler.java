package xml;

import com.jme3.math.Vector3f;
import com.jme3.scene.Mesh;
import com.jme3.scene.Node;
import com.jme3.terrain.Terrain;
import logic.Map;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.function.Consumer;

/**
 * Created by Simon Lammer on 28.04.2015.
 */
public class MapXMLHandler extends DefaultHandler {
    //region loader
    private Loader<Terrain> terrainLoader;
    private Loader<Mesh> meshLoader;
    private Loader<Node> objectLoader;
    //endregion
    //region creators
    private MapCreator mapCreator;
    //endregion
    //region tags
    private boolean tag_map;
    private boolean tag_terrain;
    private boolean tag_navmesh;
    private boolean tag_objects;
    private boolean tag_object;
    private boolean tag_position;
    private boolean tag_X;
    private boolean tag_Y;
    private boolean tag_Z;
    private boolean tag_node;
    //endregion
    //region value storage
    private Mesh mesh;
    private String value;
    private Terrain terrain;
    private ArrayList<Node> objects;
    private Vector3f vector;
    private Node model;
    //endregion
    //region callback
    private Consumer<Map> mapConsumer;
    //endregion

    public MapXMLHandler(Loader<Terrain> terrainLoader, Loader<Mesh> meshLoader, Loader<Node> objectLoader, MapCreator mapCreator, Consumer<Map> mapConsumer) {
        this.terrainLoader = terrainLoader;
        this.meshLoader = meshLoader;
        this.objectLoader = objectLoader;
        this.mapCreator = mapCreator;
        this.mapConsumer = mapConsumer;
        objects = new ArrayList<>();
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        value = new String(ch, start, length);
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        super.endElement(uri, localName, qName);
        boolean err = false;
        if (tag_map) {
            if (qName.equalsIgnoreCase("map")) {
                tag_map = false;
                Map result = mapCreator.createMap(terrain, mesh);
                mapConsumer.accept(result);
            } else if (tag_terrain && qName.equalsIgnoreCase("terrain")) {
                tag_terrain = false;
                terrain = terrainLoader.load(value);
            } else if (tag_navmesh && qName.equalsIgnoreCase("navmesh")) {
                tag_navmesh = false;
                mesh = meshLoader.load(value);
            } else if (tag_objects) {
                if (qName.equalsIgnoreCase("objects")) {
                    tag_objects = false;
                } else if(tag_object) {
                    if(qName.equalsIgnoreCase("object")){
                        tag_object = false;
                        objects.add(model);
                    }else if(tag_node && qName.equalsIgnoreCase("node")){
                        tag_node = false;
                        model = objectLoader.load(value);
                    }else if(tag_position){
                        if(qName.equalsIgnoreCase("position")){
                            tag_position = false;
                            model.setLocalTranslation(vector);
                        }else if(tag_X && qName.equalsIgnoreCase("X")){
                            tag_X = false;
                            vector.setX(Float.parseFloat(value));
                        }else if(tag_Y && qName.equalsIgnoreCase("Y")){
                            tag_Y = false;
                            vector.setY(Float.parseFloat(value));
                        }else if(tag_Z && qName.equalsIgnoreCase("Z")){
                            tag_Z = false;
                            vector.setZ(Float.parseFloat(value));
                        }else {
                            err = true;
                        }
                    }else {
                        err = true;
                    }
                }else{
                    err = true;
                }
            } else {
                err = true;
            }
        } else {
            err = true;
        }
        if (err) {
            throw new IllegalStateException("Unexpected sequence of tags. Could not close tag '" + qName + "'");
        }
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        super.startElement(uri, localName, qName, attributes);
        boolean err = false;
        if (tag_map) {
            if (!tag_terrain && qName.equalsIgnoreCase("terrain")) {
                tag_terrain = true;
            } else if (!tag_navmesh && qName.equalsIgnoreCase("navmesh")) {
                tag_navmesh = true;
            } else if (!tag_objects && qName.equalsIgnoreCase("objects")) {
                tag_objects = true;
            } else if (tag_objects) {
                if(!tag_object && qName.equalsIgnoreCase("object")){
                    tag_object = true;
                }else if(tag_object){
                    if(!tag_node && qName.equalsIgnoreCase("node")){
                        tag_node = true;
                    }else if(!tag_position && qName.equalsIgnoreCase("position")){
                        tag_position = true;
                        vector = new Vector3f();
                    }else if(tag_position){
                        if(!tag_X && qName.equalsIgnoreCase("x")){
                            tag_X = true;
                        }else if(!tag_Y && qName.equalsIgnoreCase("y")){
                            tag_Y = true;
                        }else if(!tag_Z && qName.equalsIgnoreCase("z")){
                            tag_Z = true;
                        }else{
                            err = true;
                        }
                    }else{
                        err = true;
                    }
                }
                //tag_node = true;
            } else {
                err = true;
            }
        } else if (qName.equalsIgnoreCase("map")) {
            tag_map = true;
        } else {
            err = true;
        }
        if (err) {
            throw new IllegalStateException("Unexpected sequence of tags. Could not open tag '" + qName + "'");
        }
    }
}