package xml;

import com.jme3.scene.Node;
import logic.Vehicle;

/**
 * Created by Simon Lammer on 30.04.2015.
 */
public interface VehicleCreator {
    public Vehicle createVehicle(String name, float maxFuel, String type, float speed, float swimspeed, int health, float maxInventoryWeight, float maxInventorySize, Node model, float size, float weight);
}
