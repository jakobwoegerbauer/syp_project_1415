package xml;

import com.jme3.math.Vector3f;
import javafx.util.Pair;
import logic.*;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;

/**
 * Created by Simon Lammer on 29.04.2015.
 */
public class LevelXMLHandler extends DefaultHandler {
    //region loader
    private Loader<InventoryItem> inventoryItemLoader;
    private Loader<Vehicle> vehicleLoader;
    private Loader<InfantryState> infantryStateLoader;
    //endregion
    //region creators
    private LevelCreator levelCreator;
    private PlayerCreator playerCreator;
    private UnitGroupCreator unitGroupCreator;
    //endregion
    //region tags
    private boolean tag_level;
    private boolean tag_levelName;
    private boolean tag_description;
    private boolean tag_factoryPath;
    private boolean tag_mapPath;
    private boolean tag_players;
    private boolean tag_player;
    private boolean tag_basePosition;
    private boolean tag_x;
    private boolean tag_y;
    private boolean tag_z;
    private boolean tag_inventoryItems;
    private boolean tag_item;
    private boolean tag_count;
    private boolean tag_unitGroups;
    private boolean tag_unitGroup;
    private boolean tag_name;
    private boolean tag_infantryState;
    private boolean tag_infantryCount;
    private boolean tag_vehicles;
    private boolean tag_vehicle;
    //endregion
    //region value storage
    private String value;
    private String levelName;
    private String description;
    private String factoryPath;
    private String mapPath;
    private List<Player> players;
    private Vector3f basePosition;
    private List<Pair<InventoryItem, Integer>> inventoryItems;
    private String name;
    private int count;
    private List<UnitGroup> unitGroups;
    private List<Vehicle> vehicles;
    private InfantryState infantryState;
    private int infantryCount;
    //endregion
    //region callback
    private Consumer<Level> levelConsumer;
    private Consumer<String> factoryXMLConsumer;
    private Consumer<String> mapXMLConsumer;
    //endregion

    public LevelXMLHandler(Loader<InventoryItem> inventoryItemLoader, Loader<Vehicle> vehicleLoader, LevelCreator levelCreator, PlayerCreator playerCreator, UnitGroupCreator unitGroupCreator, Consumer<Level> levelConsumer, Consumer<String> facoryXMLConsumer, Consumer<String> mapXMLConsumer, Loader<InfantryState> infantryStateLoader) {
        this.inventoryItemLoader = inventoryItemLoader;
        this.vehicleLoader = vehicleLoader;
        this.levelCreator = levelCreator;
        this.playerCreator = playerCreator;
        this.unitGroupCreator = unitGroupCreator;
        this.levelConsumer = levelConsumer;
        this.factoryXMLConsumer = facoryXMLConsumer;
        this.mapXMLConsumer = mapXMLConsumer;
        this.infantryStateLoader = infantryStateLoader;
        this.inventoryItems = new LinkedList<>();
        this.players = new LinkedList<>();
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        value = new String(ch, start, length);
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        super.endElement(uri, localName, qName);
        boolean err = false;
        if (tag_level) {
            if (qName.equalsIgnoreCase("level")) {
                tag_level = false;
                Level level = levelCreator.createLevel(levelName, description, factoryPath, mapPath, players.toArray(new Player[players.size()]));
                levelConsumer.accept(level);
            } else if (tag_levelName && qName.equalsIgnoreCase("levelname")) {
                tag_levelName = false;
                this.levelName = value;
            } else if (tag_description && qName.equalsIgnoreCase("description")) {
                tag_description = false;
                description = value;
            } else if (tag_factoryPath && qName.equalsIgnoreCase("factorypath")) {
                tag_factoryPath = false;
                factoryPath = value;
                factoryXMLConsumer.accept(factoryPath);
            } else if (tag_mapPath && qName.equalsIgnoreCase("mappath")) {
                tag_mapPath = false;
                mapPath = value;
                mapXMLConsumer.accept(mapPath);
            } else if (tag_players) {
                if (qName.equalsIgnoreCase("players")) {
                    tag_players = false;
                } else if (tag_player) {
                    if (qName.equalsIgnoreCase("player")) {
                        tag_player = false;
                        players.add(playerCreator.createPlayer(basePosition,inventoryItems,unitGroups));
                    } else if (tag_basePosition) {
                        if (qName.equalsIgnoreCase("baseposition")) {
                            tag_basePosition = false;
                        } else if (tag_x && qName.equalsIgnoreCase("x")) {
                            tag_x = false;
                            basePosition = new Vector3f();
                            basePosition.setX(Float.parseFloat(value));
                        } else if (tag_y && qName.equalsIgnoreCase("y")) {
                            tag_y = false;
                            basePosition.setY(Float.parseFloat(value));
                        } else if (tag_z && qName.equalsIgnoreCase("z")) {
                            tag_z = false;
                            basePosition.setZ(Float.parseFloat(value));
                        } else {
                            err = true;
                        }
                    } else if (tag_inventoryItems) {
                        if (qName.equalsIgnoreCase("inventoryitems")) {
                            tag_inventoryItems = false;
                        } else if (tag_item) {
                            if (qName.equalsIgnoreCase("item")) {
                                tag_item = false;
                                InventoryItem item = inventoryItemLoader.load(name);
                                inventoryItems.add(new Pair<>(item, count));
                            } else if (tag_name && qName.equalsIgnoreCase("name")) {
                                tag_name = false;
                                name = value;
                            } else if (tag_count && qName.equalsIgnoreCase("count")) {
                                tag_count = false;
                                count = Integer.parseInt(value);
                            } else {
                                err = true;
                            }
                        } else {
                            err = true;
                        }
                    } else if (tag_unitGroups) {
                        if (qName.equalsIgnoreCase("unitgroups")) {
                            tag_unitGroups = false;
                        } else if (tag_unitGroup) {
                            if (qName.equalsIgnoreCase("unitgroup")) {
                                tag_unitGroup = false;
                                UnitGroup ug = unitGroupCreator.createUnitGroup(name, infantryState, infantryCount, vehicles.toArray(new Vehicle[vehicles.size()]));
                                unitGroups.add(ug);
                            } else if (tag_name) {
                                tag_name = false;
                                name = value;
                            } else if (tag_infantryState) {
                                tag_infantryState = false;
                                infantryState = infantryStateLoader.load(value);
                            } else if (tag_infantryCount) {
                                tag_infantryCount = false;
                                infantryCount = Integer.parseInt(value);
                            } else if (tag_vehicles) {
                                if (qName.equalsIgnoreCase("vehicles")) {
                                    tag_vehicles = false;
                                } else if (tag_vehicle && qName.equalsIgnoreCase("vehicle")) {
                                    tag_vehicle = false;
                                    Vehicle v = vehicleLoader.load(value);
                                    vehicles.add(v);
                                }
                            } else {
                                err = true;
                            }
                        }
                    } else {
                        err = true;
                    }
                } else {
                    err = true;
                }
            } else {
                err = true;
            }
        } else {
            err = true;
        }
        if (err) {
            throw new IllegalStateException("Unexpected sequence of tags. Could not close tag '" + qName + "'");
        }
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        super.startElement(uri, localName, qName, attributes);
        boolean err = false;
        if (!tag_level && qName.equalsIgnoreCase("level")) {
            tag_level = true;
        } else if (tag_level) {
            if (!tag_levelName && qName.equalsIgnoreCase("levelname")) {
                tag_levelName = true;
            } else if (!tag_description && qName.equalsIgnoreCase("description")) {
                tag_description = true;
            } else if (!tag_factoryPath && qName.equalsIgnoreCase("factorypath")) {
                tag_factoryPath = true;
            } else if (!tag_mapPath && qName.equalsIgnoreCase("mappath")) {
                tag_mapPath = true;
            } else if (!tag_players && qName.equalsIgnoreCase("players")) {
                tag_players = true;
            } else if (tag_players) {
                if (!tag_player && qName.equalsIgnoreCase("player")) {
                    unitGroups = new LinkedList<>();
                    tag_player = true;
                } else if (tag_player) {
                    if (!tag_basePosition && qName.equalsIgnoreCase("baseposition")) {
                        tag_basePosition = true;
                    } else if (tag_basePosition) {
                        if (!tag_x && qName.equalsIgnoreCase("x")) {
                            tag_x = true;
                        } else if (!tag_y && qName.equalsIgnoreCase("y")) {
                            tag_y = true;
                        } else if (!tag_z && qName.equalsIgnoreCase("z")) {
                            tag_z = true;
                        } else {
                            err = true;
                        }
                    } else if (!tag_inventoryItems && qName.equalsIgnoreCase("inventoryitems")) {
                        tag_inventoryItems = true;
                    } else if (tag_inventoryItems) {
                        if (!tag_item && qName.equalsIgnoreCase("item")) {
                            tag_item = true;
                        } else if (tag_item) {
                            if (!tag_name && qName.equalsIgnoreCase("name")) {
                                tag_name = true;
                            } else if (!tag_count && qName.equalsIgnoreCase("count")) {
                                tag_count = true;
                            } else {
                                err = true;
                            }
                        } else {
                            err = true;
                        }
                    } else if (!tag_unitGroups && qName.equalsIgnoreCase("unitgroups")) {
                        tag_unitGroups = true;
                    } else if (tag_unitGroups) {
                        if (!tag_unitGroup && qName.equalsIgnoreCase("unitgroup")) {
                            tag_unitGroup = true;
                            this.vehicles = new LinkedList<>();
                        } else if (tag_unitGroup) {
                            if (!tag_name && qName.equalsIgnoreCase("name")) {
                                tag_name = true;
                            } else if (!tag_infantryState && qName.equalsIgnoreCase("infantrystate")) {
                                tag_infantryState = true;
                            } else if (!tag_infantryCount && qName.equalsIgnoreCase("infantrycount")) {
                                tag_infantryCount = true;
                            } else if (!tag_vehicles && qName.equalsIgnoreCase("vehicles")) {
                                tag_vehicles = true;
                            } else if (tag_vehicles && (!tag_vehicle && qName.equalsIgnoreCase("vehicle"))) {
                                tag_vehicle = true;
                            } else {
                                err = true;
                            }
                        } else {
                            err = true;
                        }
                    }
                } else {
                    err = true;
                }
            } else {
                err = true;
            }
        } else {
            err = true;
        }
        if (err) {
            throw new IllegalStateException("Unexpected sequence of tags. Could not open tag '" + qName + "'");
        }
    }
}
