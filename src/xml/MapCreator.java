package xml;

import com.jme3.scene.Mesh;
import com.jme3.scene.Node;
import com.jme3.terrain.Terrain;
import logic.Map;

/**
 * Created by Simon Lammer on 28.04.2015.
 */
public interface MapCreator {
    public Map createMap(Terrain terrain, Mesh mesh);
}