package xml;

import logic.BulletType;
import logic.Magazine;

/**
 * Created by Simon Lammer on 30.04.2015.
 */
public interface MagazineCreator {
    public Magazine createMagazine(String name, float size, float weight, BulletType bulletType, int maxBullets);
}