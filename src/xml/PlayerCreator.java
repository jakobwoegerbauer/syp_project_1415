package xml;

import com.jme3.math.Vector3f;
import javafx.util.Pair;
import logic.InventoryItem;
import logic.Player;
import logic.UnitGroup;

import java.util.List;

/**
 * Created by Simon Lammer on 29.04.2015.
 */
public interface PlayerCreator {
    /**
     *
     * @param basePosition
     * @param inventoryItems the integer marks the item's number of occurences
     * @param unitGroups
     * @return
     */
    public Player createPlayer(Vector3f basePosition, List<Pair<InventoryItem,Integer>> inventoryItems, List<UnitGroup> unitGroups);
}
