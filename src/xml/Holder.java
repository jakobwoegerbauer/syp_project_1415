package xml;

/**
 * Created by Simon Lammer on 29.04.2015.
 */
public class Holder<T> {
    private T value;

    public Holder() {
        this(null);
    }
    public Holder(T initialValue) {
        value = initialValue;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T newValue) {
        value = newValue;
    }
}
