package xml;

import logic.Level;
import logic.Player;

/**
 * Created by Simon Lammer on 29.04.2015.
 */
public interface LevelCreator {
    public Level createLevel(String name, String description, String factoryXMLFilename, String mapFilename, Player[] players);
}
