package xml;

import com.jme3.scene.Node;
import logic.BulletType;
import logic.Weapon;

/**
 * Created by Simon Lammer on 30.04.2015.
 */
public interface WeaponCreator {
    public Weapon createWeapon(String name, float weight, float size, float firerate, int reloadtime, BulletType[] bulletTypes, Node model, float strength, float precision);
}
