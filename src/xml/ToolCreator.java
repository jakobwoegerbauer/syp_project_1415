package xml;

import logic.Tool;

/**
 * Created by Simon Lammer on 30.04.2015.
 */
public interface ToolCreator {
    public Tool createTool(String name, float weight, float size);
}
