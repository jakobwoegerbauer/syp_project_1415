package xml;

/**
 * Created by Simon Lammer on 29.04.2015.
 */
public interface Loader<T> {
    public T load(String name);
}
