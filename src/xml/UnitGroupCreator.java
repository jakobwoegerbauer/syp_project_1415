package xml;

import logic.InfantryState;
import logic.UnitGroup;
import logic.Vehicle;

/**
 * Created by Simon Lammer on 29.04.2015.
 */
public interface UnitGroupCreator {
    public UnitGroup createUnitGroup(String name, InfantryState state, int infantryCount, Vehicle[] vehicles);
}
