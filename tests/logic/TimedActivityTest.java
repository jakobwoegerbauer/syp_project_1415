package logic;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

/**
 * Created by simonlammer on 27.12.14.
 */
public class TimedActivityTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testNull() {
        expectedException.expect(IllegalArgumentException.class);
        Activity a = new TimedActivity(null, 1000);
    }

    @Test
    public void testDurationBelowZero() {
        expectedException.expect(IllegalArgumentException.class);
        Activity a = new TimedActivity((float tpf) -> {}, -1);
    }

    @Test
    public void testDurationEqualsZero() {
        expectedException.expect(IllegalArgumentException.class);
        Activity a = new TimedActivity((float tpf) -> {}, 0);
    }

    @Test
    public void testExact() {
        // Initialisation
        SampleValue value = new SampleValue(0);
        Action action = new SampleAction(value);
        Activity a = new TimedActivity(action, 10);
        ActivityFinishedListener listener = new SampleActivityFinishedListener(value, 10);

        // add ActivityFinishedListener
        a.addActivityFinishedListener(listener);

        // Testing value & a.finished()
        assertEquals(value.getValue(), 0);
        a.update(1);
        assertFalse(a.finished());
        assertEquals(value.getValue(), 1);
        a.update(7);
        assertFalse(a.finished());
        assertEquals(value.getValue(), 8);
        assertFalse(a.finished());
        a.update(2); // exact activityFinished
        assertEquals(value.getValue(), 10);
        assertTrue(a.finished());
        a.update(15); // already finished, thus shouldn't do anything
        assertEquals(value.getValue(), 10);
        assertTrue(a.finished());
    }

    @Test
    public void testOverdo() {
        // Initialisation
        SampleValue value = new SampleValue(0);
        Action action = new SampleAction(value);
        Activity a = new TimedActivity(action, 10);
        ActivityFinishedListener listener = new SampleActivityFinishedListener(value, 10);

        // add ActivityFinishedListener
        a.addActivityFinishedListener(listener);

        // Testing
        assertEquals(value.getValue(), 0);
        a.update(1);
        assertFalse(a.finished());
        assertEquals(value.getValue(), 1);
        a.update(7);
        assertFalse(a.finished());
        assertEquals(value.getValue(), 8);
        assertFalse(a.finished());
        a.update(3); // 1 ms too much, activity only needs 10ms to complete
        assertEquals(value.getValue(), 10);
        assertTrue(a.finished());
        a.update(15); // already finished, thus shouldn't do anything
        assertEquals(value.getValue(), 10);
        assertTrue(a.finished());
    }

    private class SampleActivityFinishedListener implements ActivityFinishedListener {
        private SampleValue value;
        private int expectedValue;
        @Override
        public void handleFinishedActivity() {
            assertEquals(value.getValue(), expectedValue);
        }
        public SampleActivityFinishedListener(SampleValue value, int expectedValue) {
            this.value = value;
            this.expectedValue = expectedValue;
        }
    }
    public class SampleAction implements Action {
        private SampleValue value;
        public SampleAction(SampleValue value) {
            this.value = value;
        }
        public void performAction(float tpf) {
            this.value.setValue(this.value.getValue() + (int) tpf);
        }
    }
    private class SampleValue {
        private int value;
        public SampleValue(int value) {
            this.value = value;
        }
        public void setValue(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
}
