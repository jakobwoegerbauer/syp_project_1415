package logic;

import com.jme3.math.Vector3f;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class WeaponTest implements MagazineEmptyListener {
    private boolean magazineEmpty;

    @Test(expected=IllegalArgumentException.class)
    public void reloadNullTest(){
        Weapon w = new Weapon("w", 10, 15.2f, 1,1,1,1);
        w.reload(null);
    }

    @Test
    public void incompatibleBulletType(){
        Weapon w = new Weapon("w",10,12.5f,1,1,1,1);
        Magazine m = new Magazine("m",12,12,12,new BulletType("k",1,1,1,1),12);
        w.addCompatibleBulletType(new BulletType("k",1,2,1,1));
        assertFalse(w.reload(m));
    }

    @Test
    public void shootNull(){
        ExpectedException expectedException = ExpectedException.none();
        Weapon w = new Weapon("w", 10, 15.2f,1,1,1,1);

        Magazine m = new Magazine("m", 10,10,12,new BulletType("k",1,1.8f,9,1),12);
        w.addCompatibleBulletType(new BulletType("k",1,1.8f,7.12f,1));
        w.reload(m);

        expectedException.expect(IllegalArgumentException.class);
        w.shoot(null);
    }

    @Test
    public void shootNoBullet(){
        Weapon w = new Weapon("w",10,10,1,1,1,1);
        BulletType bt = new BulletType("k",1,1,9,1);
        w.addCompatibleBulletType(bt);
        Magazine m = new Magazine("m",5,5,0,bt,12);
        w.reload(m);

        assertFalse(w.shoot(new Vector3f(2, 1, 6)));
    }

    @Test
    public void shootOk() throws InterruptedException {
        magazineEmpty = false;
        Weapon w = new Weapon("w",10,10,100,1,1,1);
        w.addMagazineEmptyListener(this);
        BulletType bt = new BulletType("k",1,1,9,1);
        w.addCompatibleBulletType(bt);
        Magazine m = new Magazine("m",5,5,12,bt,12);
        w.reload(m);
        Thread.sleep(15);
        for(int i = 0; i < 12; i++) {
            assertTrue(w.shoot(new Vector3f(5, 1, 2)));
            Thread.sleep(15);
        }
        assertFalse(w.shoot(new Vector3f(5,5,5)));
        assertFalse(m.canShoot());

        assertTrue(magazineEmpty);
        magazineEmpty = false;

        m = new Magazine("m2", 5,5,12,bt,12);
        w.reload(m);
        Thread.sleep(15);
        for(int i = 0; i < 12; i++) {
            assertTrue(w.shoot(new Vector3f(5, 1, 2)));
            Thread.sleep(15);
        }
        assertFalse(w.shoot(new Vector3f(5,5,5)));
        assertFalse(m.canShoot());
        assertTrue(magazineEmpty);
    }

    @Test
    public void shootTooFast() throws InterruptedException {
        Weapon w = new Weapon("w",10,10,10,1000,1,1);
        w.addMagazineEmptyListener(this);
        BulletType bt = new BulletType("k",1,1,9,1);
        w.addCompatibleBulletType(bt);
        Magazine m = new Magazine("m",5,5,12,bt,12);
        assertTrue(w.reload(m));

        assertFalse(w.shoot(new Vector3f(2,2,2)));
        Thread.sleep(1000);
        assertTrue(w.shoot(new Vector3f(2,2,2)));

        assertFalse(w.shoot(new Vector3f(2,2,2)));
        Thread.sleep(100);
        assertTrue(w.shoot(new Vector3f(2,2,2)));
    }

    @Override
    public void handleMagazineEmpty() {
        magazineEmpty = true;
    }
}