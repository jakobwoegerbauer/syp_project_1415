package logic;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class MagazineTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void nameEqualsNull() {
        expectedException.expect(IllegalArgumentException.class);
        Magazine m = new Magazine(null, 5,5,12,new BulletType("k",1,1,9,1),12);
    }

    @Test
    public void bulletTypeEqualsNull() {
        expectedException.expect(IllegalArgumentException.class);
        Magazine m = new Magazine("mag", 5,2,12,null,12);
    }

    @Test
    public void bulletCountBiggerMaxBullets(){
        expectedException.expect(IllegalArgumentException.class);
        Magazine m = new Magazine("mag", 5,10,13,new BulletType("k",2,1,3,1),12);
    }

    @Test
    public void getValues(){
        BulletType b = new BulletType("k",1.5f,1.8f, 7.12f,1);
        Magazine m = new Magazine("mag", 14, 10, 7, b, 12);

        assertTrue(m.getName().equals("mag"));
        assertTrue(m.getSize() == 14);
        assertTrue(m.getWeight() == 10);
        assertTrue(m.getBulletCount() == 7);
        assertTrue(m.getBulletType().equals(b));
        assertTrue(m.getMaxBullets() == 12);
    }

    @Test
    public void testShoot(){
        BulletType b = new BulletType("k",1.5f,1.8f, 9,1);
        Magazine m = new Magazine("mag", 14, 12, 12, b, 12);
        assertTrue(m.getBulletCount() == 12);

        for(int i = 12; i > 0; i--) {
            assertTrue(m.canShoot());
            assertTrue(m.shoot());
            assertTrue(m.getBulletCount() == i-1);
        }

        assertFalse(m.canShoot());
    }

    @Test(expected = UnsupportedOperationException.class)
    public void shootNotPossible(){
        BulletType b = new BulletType("k",1.5f,1.8f, 9,1);
        Magazine m = new Magazine("mag", 14, 12, 0, b, 12);
        m.shoot();
    }
}