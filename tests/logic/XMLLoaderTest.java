package logic;

import org.junit.Rule;
import org.junit.rules.ExpectedException;

/**
 * Created by simonlammer on 05.04.15.
 */
public class XMLLoaderTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

   /* @Test
    public void test() {
        double delta = 0.00001;
        try {
            XMLLoader.loadFactoryXML("organizational/xml-formate/FactoryXMLFormate_neu.xml", new PathFinder());

            // test magazines
            List<Magazine> magazines = XMLLoader.getMagazines();
            assertEquals(1, magazines.size());
            Magazine magazine = magazines.get(0);
            assertEquals("9mm Pistolenmunition", magazine.getName());
            assertEquals(0.6, magazine.getWeight(), delta);
            assertEquals(2.4, magazine.getSize(), delta);
            // TODO check BulletType
            assertEquals(30, magazine.getBulletCount());

            // test tools
            List<Tool> tools = XMLLoader.getTools();
            assertEquals(1, tools.size());
            Tool tool = tools.get(0);
            assertEquals("Medikit", tool.getName());
            assertEquals(4.5, tool.getWeight(), delta);
            assertEquals(0.85, tool.getSize(), delta);

            // test vehicles
            List<Vehicle> vehicles = XMLLoader.getVehicles();
            assertEquals(1, vehicles.size());
            Vehicle vehicle = vehicles.get(0);
            assertEquals("Jakob", vehicle.getName());
            assertEquals(100.0, vehicle.getFuel(), delta);
            // TODO test Weapon
            assertEquals(54.6, vehicle.getSpeed(), delta);
            //assertEquals(78.34, vehicle.getSwimSpeed(), delta);

            // test weapons
            List<Weapon> weapons = XMLLoader.getWeapons();
            assertEquals(1, weapons.size());
            Weapon weapon = weapons.get(0);
            assertEquals("M46", weapon.getName());
            assertEquals(2.7, weapon.getWeight(), delta);
            assertEquals(50.5, weapon.getSize(), delta);
            assertEquals(4.0f, weapon.getFireRate(), delta);
            assertEquals(2500, weapon.getReloadTime());
            // TODO test BulletType
        } catch (ParserConfigurationException e) {
            fail("ParserConfigurationException: " + e.getMessage());
        } catch (SAXException e) {
            fail("SAXException: " + e.getMessage());
        } catch (IOException e) {
            fail("IOException: " + e.getMessage());
        }
    }*/
}
