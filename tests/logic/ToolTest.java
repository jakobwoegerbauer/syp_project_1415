package logic;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ToolTest {

    @Test(expected = IllegalArgumentException.class)
    public void nameEqualsNull() throws Exception {
        Tool t = new Tool(null, 10,10);
    }

    @Test(expected = IllegalArgumentException.class)
    public void sizeLessThanZero(){
        Tool t = new Tool("t",-5,10);
    }

    @Test(expected = IllegalArgumentException.class)
    public void weightLessThanZero(){
        Tool t = new Tool("t", 10, -5);
    }

    @Test
    public void toolOk(){
        Tool t = new Tool("t", 10,5.5f);
        assertTrue(t.getSize() == 10);
        assertTrue(t.getWeight() == 5.5f);
        assertTrue(t.getName().equals("t"));
    }

    @Test
    public void notEqual(){
        Tool t1 = new Tool("t1", 5, 5);
        Tool t2 = new Tool("t2", 5, 5);
        assertFalse(t1.equals(t2));
        assertFalse(t2.equals(t1));
    }

    @Test
    public void testEquals(){
        Tool t1 = new Tool("t", 2, 5);
        Tool t2 = new Tool("t", 8,20);
        assertTrue(t1.equals(t2));
        assertTrue(t2.equals(t1));
    }
}