package logic;

import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.shape.Box;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class ShotTest {

    @Test(expected = IllegalArgumentException.class)
    public void positionNull() throws Exception {
        Shot s = new Shot(null, new Vector3f(2, 2, 2), new BulletType("k",1, 1, 1, 1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void directionNull() throws Exception {
        Shot s = new Shot(new Vector3f(2, 2, 2), null, new BulletType("k",1, 1, 1, 1));
    }

    @Test(expected = IllegalArgumentException.class)
    public void bulletTypeNull() throws Exception {
        Shot s = new Shot(new Vector3f(2, 2, 2), new Vector3f(2, 2, 2), null);
    }

    @Test
    public void addDriftTestSimple() {
        Vector3f v = new Vector3f(1000, 0, 0);
        Vector3f v1 = Shot.addDrift(v, 3);
        Vector3f dif = v1.subtract(v);

        assertTrue(Math.abs(v.length() - v1.length()) < 0.001);
        assertTrue(Shot.radiantToMil(v.normalize().angleBetween(v1.normalize())) < 3);
    }

    @Test
    public void addDriftTest() {
        Vector3f v = new Vector3f(1000, 1000, 1000);
        Vector3f v1 = Shot.addDrift(v, 50);
        Vector3f dif = v1.subtract(v);

        assertTrue(Math.abs(v.length() - v1.length()) < 0.001);
        assertTrue(Shot.radiantToMil(v.normalize().angleBetween(v1.normalize())) < 50);
    }

    @Test
    public void addDriftTestNegative() {
        Vector3f v = new Vector3f(1000, -1000, -500);
        Vector3f v1 = Shot.addDrift(v, 5);
        Vector3f dif = v1.subtract(v);

        assertTrue(Math.abs(v.length() - v1.length()) < 0.001);
        assertTrue(Shot.radiantToMil(v.normalize().angleBetween(v1.normalize())) < 5);
    }

    @Test
    public void shotTest1() {
        Shot s = new Shot(Vector3f.ZERO, Vector3f.UNIT_X.mult(880), new BulletType("k",0.060f, 5f, 2f, 1f));
        List<Unit> unitList = new LinkedList<Unit>();
        Node n = new Node();

        for (int i = 0; i < 60; i++) {
            s.update(16f, unitList, n, Vector3f.NEGATIVE_INFINITY, Vector3f.POSITIVE_INFINITY);
            //System.out.println(s.getPosition().getP1() + "\t" + s.getPosition().getP2() + "\t" + s.getPosition().getZ() + "\t|\t" + s.getDirection().getP1() + "\t" + s.getDirection().getP2() + "\t" + s.getDirection().getZ());
        }
        System.out.println(s.getPosition().getX() + "\t" + s.getPosition().getY() + "\t" + s.getPosition().getZ());
        assertTrue(s.getPosition().getY() >= -6 && s.getPosition().getY() <= -4);
        assertTrue(s.getPosition().getX() >= 840 && s.getPosition().getX() <= 850);
        assertTrue(s.getPosition().getZ() >= -0.8 && s.getPosition().getZ() <= 0.8);
    }

    @Test
    public void shotTest2() {
        Shot s = new Shot(Vector3f.ZERO, new Vector3f(10, 10, 0), new BulletType("k",1f, 1f, 2f, 20f));
        List<Unit> unitList = new LinkedList<Unit>();
        Node n = new Node();

        for (int i = 0; i < 120; i++) {
            s.update(16f, unitList, n, Vector3f.NEGATIVE_INFINITY, Vector3f.POSITIVE_INFINITY);
            //System.out.println(((double)i/60.0) + ": " + s.getPosition().getX() + "\t" + s.getPosition().getY() + "\t" + s.getPosition().getZ() + "\t|\t" + s.getDirection().getX() + "\t" + s.getDirection().getY() + "\t" + s.getDirection().getZ());
        }
        System.out.println(s.getPosition().getX() + "\t" + s.getPosition().getY() + "\t" + s.getPosition().getZ());
        assertTrue(s.getPosition().getY() >= 0.5 && s.getPosition().getY() <= 1.5);
        assertTrue(s.getPosition().getX() >= 18.5 && s.getPosition().getX() <= 19.5);
        assertTrue(s.getPosition().getZ() >= -0.4 && s.getPosition().getZ() <= 0.4);
    }

    @Test
    public void shotOutOfBounds() {
        BulletType myBulletType = new BulletType("k",0.060f, 5f, 2f, 1f);
        Shot s = new Shot(new Vector3f(10, 10, 10), Vector3f.UNIT_X.mult(880), myBulletType);
        List<Unit> unitList = new LinkedList<Unit>();
        Node n = new Node();
        MyOnImpactListener onImpactListener = new MyOnImpactListener();
        s.addOnImpactListener(onImpactListener);

        s.update(1000f, unitList, n, Vector3f.ZERO, new Vector3f(200, 200, 200));
        assertTrue(onImpactListener.impact);
        assertTrue(onImpactListener.unit == null);
        assertTrue(onImpactListener.bulletType.equals(myBulletType));
    }

    @Test
    public void ShotAtBox() {
        Box b = new Box(1, 10, 10);
        Node node1 = new Node();

        // Target in 500m distance
        Geometry g1 = new Geometry("Box1", b);
        g1.setLocalTranslation(new Vector3f(800, -5, -5));
        g1.updateModelBound();
        node1.attachChild(g1);
        node1.updateModelBound();
        node1.updateGeometricState();

        Shot s = new Shot(Vector3f.ZERO, Vector3f.UNIT_X.mult(880), new BulletType("k",0.060f, 5f, 2f, 1f));
        List<Unit> unitList = new LinkedList<Unit>();

        MyOnImpactListener listener = new MyOnImpactListener();
        s.addOnImpactListener(listener);

        int cnt = 0;
        while (!listener.impact && cnt < 60) {
            s.update(16f, unitList, node1, Vector3f.NEGATIVE_INFINITY, Vector3f.POSITIVE_INFINITY);
            cnt++;
        }
        assertTrue(listener.unit == null);
        assertTrue(cnt > 55 && cnt < 65);
    }

    @Test
    public void ShotAtUnit() {
        Box b = new Box(1, 10, 10);
        Node node = new Node();
        Geometry g1 = new Geometry("Box1", b);
        node.attachChild(g1);


        Unit u = new Infantry("jakob", 2.4, 2.0, 45.0, 1.9, 100, new Inventory(30,1.8), node);
        u.setInstantPosition(new Vector3f(800,-5,-5));

        Shot s = new Shot(Vector3f.ZERO, Vector3f.UNIT_X.mult(880), new BulletType("k",0.060f, 5f, 2f, 1f));
        List<Unit> unitList = new LinkedList<Unit>();
        unitList.add(u);

        MyOnImpactListener listener = new MyOnImpactListener();
        s.addOnImpactListener(listener);

        int cnt = 0;
        while (!listener.impact && cnt < 66) {
            s.update(16f, unitList, new Node(), Vector3f.NEGATIVE_INFINITY, Vector3f.POSITIVE_INFINITY);
            cnt++;
        }
        assertTrue(cnt > 55 && cnt < 65);
        assertTrue(listener.unit == u);
    }

    private class MyOnImpactListener implements OnImpactListener{
        Unit unit;
        BulletType bulletType;
        Vector3f position;
        Vector3f direction;
        boolean impact = false;

        @Override
        public void onImpact(Shot shot, Unit unit) {
            impact = true;
            this.bulletType = shot.getBulletType();
            this.position = shot.getPosition();
            this.direction = shot.getDirection();
            this.unit = unit;
        }
    }

}