package logic;

/**
 * Created by simonlammer on 25.12.14.
 */
public class UnitGroupTest {
    private String name;

    private void setName() {
        if (name == null || name.length() < UnitGroup.MIN_NAME_LENGTH || name.length() > UnitGroup.MAX_NAME_LENGTH) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < UnitGroup.MIN_NAME_LENGTH; i++) {
                sb.append('x');
            }
            name = sb.toString();
        }
    }
}
