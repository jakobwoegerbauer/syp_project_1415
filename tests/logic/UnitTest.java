package logic;

import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import junit.framework.Assert;

import java.util.List;

/**
 * Created by Philipp on 04.02.2015.
 */
public class UnitTest { // TODO: add "@Test" annotation to each test-method

    public UnitTest() {
    }

    @org.junit.Test
    public void testCreateNewUnit() throws Exception {
        try {
            Unit u = new MyUnit(null, 0, 0, 0, 0, 0, null, null);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    public void testAddAndCanAddItem() throws Exception {
        Unit u = new MyUnit("Tank1", 5.5, 10.4, 1000, 1, 2000, new Inventory(2000, 10), new Node("Node1"));
        InventoryItem test = new InventoryItem() {
            @Override
            public String getName() {
                return "HealthPackage";
            }

            @Override
            public double getSize() {
                return 2;
            }

            @Override
            public double getWeight() {
                return 1;
            }

            @Override
            public InventoryItem clone() {
                return this;
            }
        };
        try {
            if (u.canAddItem(test, u.inventory))
                u.addItem(test);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    public void testAddMoveListener() throws Exception {
        Unit u = new MyUnit("Tank1", 5.5, 10.4, 1000, 1, 2000, new Inventory(2000, 10), new Node("Node1"));
        MoveListener testListener = new MoveListener() {
            @Override
            public void handleMoveEvent(Vector3f position) {
                position.add(3, 4, 4); //JAKOB: is des richtig? ka ahnung von vector3f
            }
        };
        u.addMoveListener(testListener);
    }

    public void testGetItems() throws Exception {
        Unit u = new MyUnit("Tank1", 5.5, 10.4, 1000, 1, 2000, new Inventory(2000, 10), new Node("Node1"));
        u.addItem(new InventoryItem() {
            @Override
            public String getName() {
                return "Healthpackage";
            }

            @Override
            public double getSize() {
                return 2;
            }

            @Override
            public double getWeight() {
                return 1;
            }

            @Override
            public InventoryItem clone() {
                return this;
            }
        });

        u.addItem(new InventoryItem() {
            @Override
            public String getName() {
                return "MG";
            }

            @Override
            public double getSize() {
                return 20;
            }

            @Override
            public double getWeight() {
                return 200;
            }

            @Override
            public InventoryItem clone() {
                return this;
            }
        });

        List<InventoryItem> actual = u.getItems();
        Assert.assertEquals(2, actual.size());
    }

    public void testRemoveItem() throws Exception {
        Unit u = new MyUnit("Tank1", 5.5, 10.4, 1000, 1, 2000, new Inventory(2000, 10), new Node("Node1"));
        InventoryItem health = new InventoryItem() {
            @Override
            public String getName() {
                return "Healthpackage";
            }

            @Override
            public double getSize() {
                return 2;
            }

            @Override
            public double getWeight() {
                return 1;
            }

            @Override
            public InventoryItem clone() {
                return this;
            }
        };

        u.addItem(health);
        u.addItem(new InventoryItem() {
            @Override
            public String getName() {
                return "MG";
            }

            @Override
            public double getSize() {
                return 20;
            }

            @Override
            public double getWeight() {
                return 200;
            }

            @Override
            public InventoryItem clone() {
                return this;
            }
        });

        Assert.assertEquals(2, u.getItems().size());

        try {
            u.removeItem(health, 1);
            Assert.assertEquals(1, u.getItems().size());
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    class MyUnit extends Unit {
        public MyUnit(String name, double speed, double size, double weight, int swimSpeed, int health, Inventory inventory, Node model) {
            super(name, speed, size, weight, swimSpeed, health, inventory, model);
        }

        @Override
        public Unit clone() {
            return this;
        }
    }
}