package logic;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertTrue;

/**
 * Created by simonlammer on 25.12.14.
 */
public class UnitGroupNameTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testNameNull() {
        expectedException.expect(IllegalArgumentException.class);
        UnitGroup u = new UnitGroup(null);
    }

    @Test
    public void testNameShort() {
        if (UnitGroup.MIN_NAME_LENGTH == 0) {
            assertTrue("Minimum length is set to 0, thus can't test behaviour", true);
        } else {
            expectedException.expect(IllegalArgumentException.class);
            UnitGroup u = new UnitGroup("");
        }
    }

    @Test
    public void testNameLong() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i <= UnitGroup.MAX_NAME_LENGTH; i++) {
            sb.append('x');
        }
        expectedException.expect(IllegalArgumentException.class);
        UnitGroup u = new UnitGroup(sb.toString());
    }

    @Test
    public void testNameValid() {
        UnitGroup u;
        StringBuilder sb = new StringBuilder();
        for (int i = 1; i < UnitGroup.MIN_NAME_LENGTH; i++) {
            sb.append('x');
        }
        for (int i = UnitGroup.MIN_NAME_LENGTH; i <= UnitGroup.MAX_NAME_LENGTH; i++) {
            sb.append('x');
            u = new UnitGroup(sb.toString());
        }
    }
}
