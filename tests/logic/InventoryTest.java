package logic;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class InventoryTest {

    @Test
    public void testWeight() throws Exception {
        Inventory i = new Inventory(10, 50);
        assertTrue("Empty Inventory has weight 0", i.getWeight() == 0);

        Tool t1 = new Tool("Tool1", 1, 2);
        assertTrue(i.canAdd(t1));
        assertTrue(i.addItem(t1));
        assertTrue(i.hasItem(t1) == 1);
        assertTrue("Weight should be 2", 2 == t1.getWeight());

        Tool t2 = new Tool("Tool2", 1, 9);
        assertFalse("Can't add Tool2", i.canAdd(t2));
        assertFalse("Can't add Tool2", i.addItem(t2));
        assertTrue("Weight should be 2", i.getWeight() == 2);

        Tool t3 = new Tool("Tool3", 1, 3);
        assertTrue(i.canAdd(t3));
        assertTrue(i.addItem(t3));
        assertTrue(i.getWeight() == 2 + 3);

        List<InventoryItem> l = i.getItems();
        assertTrue("Tool1 and Tool3 should be in the inventory", l.size() == 2 && l.contains(t1) && l.contains(t3));

        assertTrue(i.removeItem(t2) == null);

        Tool t4 = new Tool("Tool4", 1, 6);

        assertTrue(i.removeItem(t3) == t3);
        assertTrue(i.getWeight() == 2);
        assertTrue(i.canAdd(t4));
        assertTrue(i.addItem(t4));
        assertTrue(i.getWeight() == 6 + 2);
        assertFalse(i.canAdd(t3));

        l = i.getItems();
        assertTrue(l.size() == 2 && l.contains(t1) && l.contains(t4));

        assertTrue(i.removeItem(t1) == t1);
        assertTrue(i.removeItem(t4) == t4);

        assertTrue(i.getWeight() == 0);
        assertTrue(i.getItems().size() == 0);
    }

    @Test
    public void testSize() throws Exception{
        Inventory i = new Inventory(50, 10);
        assertTrue("Empty Inventory has weight 0", i.getSize() == 0);

        Tool t1 = new Tool("Tool1", 2, 1);
        assertTrue(i.canAdd(t1));
        assertTrue(i.addItem(t1));
        assertTrue("Weight should be 2", 2 == t1.getSize());

        Tool t2 = new Tool("Tool2", 9, 1);
        assertFalse("Can't add Tool2", i.canAdd(t2));
        assertFalse("Can't add Tool2", i.addItem(t2));
        assertTrue("Weight should be 2", i.getSize() == 2);

        Tool t3 = new Tool("Tool3", 3, 1);
        assertTrue(i.canAdd(t3));
        assertTrue(i.addItem(t3));
        assertTrue(i.getSize() == 2 + 3);

        List<InventoryItem> l = i.getItems();
        assertTrue("Tool1 and Tool3 should be in the inventory", l.size() == 2 && l.contains(t1) && l.contains(t3));

        assertTrue(i.removeItem(t2) == null);

        Tool t4 = new Tool("Tool4", 6, 1);

        assertTrue(i.removeItem(t3) == t3);
        assertTrue(i.getSize() == 2);
        assertTrue(i.canAdd(t4));
        assertTrue(i.addItem(t4));
        assertTrue(i.getSize() == 6 + 2);
        assertFalse(i.canAdd(t3));

        l = i.getItems();
        assertTrue(l.size() == 2 && l.contains(t1) && l.contains(t4));

        assertTrue(i.removeItem(t1) == t1);
        assertTrue(i.removeItem(t4) == t4);

        assertTrue(i.getSize() == 0);
        assertTrue(i.getItems().size() == 0);
    }

    @Test
    public void hasItemTest() {
        Inventory i = new Inventory(10, 10);
        Tool t1 = new Tool("t1", 1, 1);
        assertTrue(i.hasItem(t1) == 0);
        i.addItem(t1);
        assertTrue(i.hasItem(t1) == 1);
        i.addItem(t1);
        assertTrue(i.hasItem(t1) == 2);
        assertTrue(i.removeItem(t1) == t1);
        assertTrue(i.hasItem(t1) == 1);
        assertTrue(i.removeItem(t1) == t1);
        assertTrue(i.hasItem(t1) == 0);
    }
}