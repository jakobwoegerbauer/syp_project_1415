package logic;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class BulletTypeTest {

    @Test
    public void testBulletType() throws Exception {
        BulletType bulletType = new BulletType("k",10,3,5,1);
        assertTrue(bulletType.getWeight() == 10);
        assertTrue(bulletType.getDamage() == 3);
        assertTrue(bulletType.getCaliber() == 5);

        BulletType b1 = new BulletType("k",5,5.1f,2,1);
        BulletType b2 = new BulletType("k",5,5.1f,2,1);
        BulletType b3 = new BulletType("k",2, 5.1f,2,1);
        BulletType b4 = new BulletType("k",5, 3,2,1);

        assertTrue(b1.equals(b2));
        assertTrue(b2.equals(b1));

        assertFalse(b1.equals(b3));
        assertFalse(b3.equals(b1));

        assertFalse(b1.equals(b4));
        assertFalse(b4.equals(b1));
    }
}