package logic;
import com.jme3.math.Vector3f;
import org.junit.*;
import org.junit.rules.ExpectedException;

/**
 * Created by simonlammer on 27.12.14.
 */
public class BaseTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testMaxHealthEqualsZero() {
        expectedException.expect(IllegalArgumentException.class);
        Base b = new Base((short)0, null, null);
    }

    @Test
    public void testPositionEqualsNull() {
        expectedException.expect(IllegalArgumentException.class);
        Base b = new Base((short)1, null, null);
    }

    @Test
    public void testInventoryEqualsNull() {
        expectedException.expect(IllegalArgumentException.class);
        Base b = new Base((short)1, null, new Vector3f(1,2,3));
    }
}
