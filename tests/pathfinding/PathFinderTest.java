package pathfinding;


import com.jme3.app.SimpleApplication;
import com.jme3.math.Vector3f;
import com.jme3.scene.*;
import com.jme3.scene.plugins.blender.BlenderModelLoader;
import com.jme3.util.BufferUtils;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class PathFinderTest{

    @Test
    public void testSmall2D() throws Exception {
        Vector3f[] vertices = new Vector3f[9];
        vertices[0] = new Vector3f(5.4f,0,7.3f);
        vertices[1] = new Vector3f(11.8f,0,4.8f);
        vertices[2] = new Vector3f(11.2f,0,10.2f);
        vertices[3] = new Vector3f(13.4f,0,6.3f);
        vertices[4] = new Vector3f(14.2f,0,4.6f);
        vertices[5] = new Vector3f(17,0,7.2f);
        vertices[6] = new Vector3f(22.1f,0,11.1f);
        vertices[7] = new Vector3f(19.3f,0,4.5f);
        vertices[8] = new Vector3f(17,0,1.1f);

        int [] indexes = {
                0,1,2,
                1,3,2,
                1,4,3,
                4,5,3,
                5,6,3,
                5,7,6,
                8,7,5};

        Mesh m = new Mesh();
        m.setBuffer(VertexBuffer.Type.Position, 3, BufferUtils.createFloatBuffer(vertices));
        m.setBuffer(VertexBuffer.Type.Index,    3, BufferUtils.createIntBuffer(indexes));
        m.updateBound();

        PathFinder p = new PathFinder();
        p.initialize(m);

        Vector3f start = new Vector3f(7.7f, 0, 6.9f);
        Vector3f end = new Vector3f(17.7f,0,3);

        try {
            MyPathFinderCallback callback = new MyPathFinderCallback();
            p.getInfantryPath(start, end, callback, "Test");

            int cnt = 0;
            while(cnt < 500 && !callback.called){
                Thread.sleep(5);
                cnt++;
            }

            assertTrue(cnt < 500);

            assertTrue(callback.points.size() == 4);
            assertTrue(callback.points.get(0).equals(start));
            assertTrue(callback.points.get(3).equals(end));
            assertTrue(callback.data.equals("Test"));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void testSmall3D() throws Exception {
        Vector3f[] vertices = new Vector3f[9];
        vertices[0] = new Vector3f(5.4f,2.6f,7.3f);
        vertices[1] = new Vector3f(11.8f,1,4.8f);
        vertices[2] = new Vector3f(11.2f,8,10.2f);
        vertices[3] = new Vector3f(13.4f,1,6.3f);
        vertices[4] = new Vector3f(14.2f,6,4.6f);
        vertices[5] = new Vector3f(17,3.3f,7.2f);
        vertices[6] = new Vector3f(22.1f,4.2f,11.1f);
        vertices[7] = new Vector3f(19.3f,0.5f,4.5f);
        vertices[8] = new Vector3f(17,1.8f,1.1f);

        int [] indexes = {
                0,1,2,
                1,3,2,
                1,4,3,
                4,5,3,
                5,6,3,
                5,7,6,
                8,7,5};

        Mesh m = new Mesh();
        m.setBuffer(VertexBuffer.Type.Position, 3, BufferUtils.createFloatBuffer(vertices));
        m.setBuffer(VertexBuffer.Type.Index,    3, BufferUtils.createIntBuffer(indexes));
        m.updateBound();

        PathFinder p = new PathFinder();
        p.initialize(m);

        Vector3f start = new Vector3f(7.7f, 0, 6.9f);
        Vector3f end = new Vector3f(17.7f, 0, 3);

        try {

            MyPathFinderCallback callback = new MyPathFinderCallback();
            p.getInfantryPath(start, end, callback, "Test");

            int cnt = 0;
            while(cnt < 500 && !callback.called){
                Thread.sleep(5);
                cnt++;
            }

            assertTrue(cnt < 500);

            assertTrue(callback.points.size() == 4);
            assertTrue(callback.points.get(0).equals(start));
            assertTrue(callback.points.get(3).equals(end));
            assertTrue(callback.data.equals("Test"));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void test2D() throws Exception {
        Vector3f[] vertices = new Vector3f[9];
        vertices[0] = new Vector3f(5.4f,0,7.3f);
        vertices[1] = new Vector3f(11.8f,0,4.8f);
        vertices[2] = new Vector3f(11.2f,0,10.2f);
        vertices[3] = new Vector3f(13.4f,0,6.3f);
        vertices[4] = new Vector3f(14.2f,0,4.6f);
        vertices[5] = new Vector3f(17,0,7.2f);
        vertices[6] = new Vector3f(22.1f,0,11.1f);
        vertices[7] = new Vector3f(19.3f,0,4.5f);
        vertices[8] = new Vector3f(17,0,1.1f);

        int [] indexes = {
                0,1,2,
                1,3,2,
                1,4,3,
                4,5,3,
                5,6,3,
                5,7,6,
                8,7,5};

        Mesh m = new Mesh();
        m.setBuffer(VertexBuffer.Type.Position, 3, BufferUtils.createFloatBuffer(vertices));
        m.setBuffer(VertexBuffer.Type.Index,    3, BufferUtils.createIntBuffer(indexes));
        m.updateBound();

        PathFinder p = new PathFinder();
        p.initialize(m);

        Vector3f start = new Vector3f(7.7f, 0,6.9f);
        Vector3f end = new Vector3f(17.7f,0,3);

        try {
            MyPathFinderCallback callback = new MyPathFinderCallback();
            p.getInfantryPath(start, end, callback, "Test");

            int cnt = 0;
            while(cnt < 500 && !callback.called){
                Thread.sleep(5);
                cnt++;
            }

            assertTrue(cnt < 500);

            assertTrue(callback.points.size() == 4);
            assertTrue(callback.points.get(0).equals(start));
            assertTrue(callback.points.get(3).equals(end));
            assertTrue(callback.data.equals("Test"));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void testMesh() throws InterruptedException {
        Tester t = new Tester();
        t.setShowSettings(false);
        t.start();
        while(!t.finished){
            Thread.sleep(10);
        }
        t.requestClose(false);
    }

    private class Tester extends SimpleApplication {
        public boolean finished = false;
        @Override
        public void simpleInitApp() {
            assetManager.registerLoader(BlenderModelLoader.class, "blend");
            Spatial s = assetManager.loadModel("testNavigationMeshes/Mesh.mesh.xml");

            Mesh m = ((Geometry)((Node)s).getChild(0)).getMesh();

            PathFinder p = new PathFinder();

            p.initialize(m);


            Vector3f start = new Vector3f(-7,0,-5);
            Vector3f end = new Vector3f(4,0,-7.3f);
            MyPathFinderCallback callback = new MyPathFinderCallback();
            p.getInfantryPath(start,end,callback,null);

            try {
                int cnt = 0;
                while (cnt < 500 && !callback.called) {
                    Thread.sleep(5);
                    cnt++;
                }
                List<Vector3f> points = callback.points;
                Vector3f[] a = (Vector3f[])points.toArray();
                assertTrue(cnt < 500);

            }catch (Exception e){
                e.printStackTrace();
            }
            finished = true;
        }
    }

    private class MyPathFinderCallback implements PathFindingCallback{
        List<Vector3f> points;
        Object data;
        boolean called = false;

        @Override
        public void pathFound(List<Vector3f> points, Object data) {
            if(called){
                Assert.fail("pathFound should be called only once");
            }

            this.points = points;
            this.data = data;
            this.called = true;
        }
    }
}