package xmlparsing;

import org.junit.Test;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by simonlammer on 14.03.15.
 */
public class XMLParserTest {
    private static SAXHandler handler;
    private static String xmlString;
    static {
        handler = new SAXHandler();
        xmlString =
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + "\r\n" +
            "<root>                                    " + "\r\n" +
            "	<subnode>                              " + "\r\n" +
            "		<id>s1</id>                        " + "\r\n" +
            "		<name>Subnode one</name>           " + "\r\n" +
            "		<value>11</value>                  " + "\r\n" +
            "	</subnode>                             " + "\r\n" +
            "	<subnode>                              " + "\r\n" +
            "		<id>s2</id>                        " + "\r\n" +
            "		<name>Subnode two</name>           " + "\r\n" +
            "		<value>22</value>                  " + "\r\n" +
            "	</subnode>                             " + "\r\n" +
            "</root>                                   ";
    }
    private SAXParser parser;

    public XMLParserTest() throws ParserConfigurationException, SAXException {
        this.parser = newSAXParser();
    }

    private SAXParser newSAXParser() throws ParserConfigurationException, SAXException {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser parser = factory.newSAXParser(); // throws ParserConfigurationException
        return parser;
    }

    @Test
    public void SAXParserNotNullTest() {
        assertNotNull(parser);
    }

    @Test
    public void SAXHandlerNotNullTest() {
        assertNotNull(handler);
    }

    @Test
    public void ParseFileTest() throws IOException, SAXException {
        // create File
        File file = new File("temporaryXMLFileUsedForUnitTest.xml");
        try {
            FileWriter fw = new FileWriter(file);
            fw.write(xmlString);
            fw.flush();
            fw.close();
        } catch (IOException e) {
            fail("Could not create file");
        }

        // parse
        handler.clearElements();
        parser.parse(file, handler);
        List<SampleElement> elements = handler.getElements();

        // validate
        validateElementList(elements);

        // delete file
        file.delete();
    }

    @Test
    public void ParseStringTest() throws IOException, SAXException {
        // parse
        handler.clearElements();
        parser.parse(new InputSource(new StringReader(xmlString)), handler);
        List<SampleElement> elements = handler.getElements();

        // validate
        validateElementList(elements);
    }

    private void validateElementList(List<SampleElement> elements) {
        assertEquals(2,             elements.size());
        assertEquals("s1",          elements.get(0).getId());
        assertEquals("Subnode one", elements.get(0).getName());
        assertEquals(11,            elements.get(0).getValue());
        assertEquals("s2",          elements.get(1).getId());
        assertEquals("Subnode two", elements.get(1).getName());
        assertEquals(22,            elements.get(1).getValue());
    }

    public static class SampleElement {
        private String id;
        private String name;
        private int value;

        public SampleElement(String id, String name, int value) {
            this.id = id;
            this.name = name;
            this.value = value;
        }

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public int getValue() {
            return value;
        }
    }

    public static class SAXHandler extends DefaultHandler {
        private LinkedList<SampleElement> elements;
        private String id;
        private String name;
        private boolean tagId = false;
        private boolean tagName = false;
        private boolean tagValue = false;
        private String value;

        public SAXHandler() {
            elements = new LinkedList<>();
        }

        public void clearElements() {
            elements.clear();
        }

        @Override
        public void characters(char[] ch, int start, int length) throws SAXException {
            String str = new String(ch, start, length);
            if (tagId) {
                id = str;
            } else if (tagName) {
                name = str;
            } else if (tagValue) {
                value = str;
            }
        }

        public List<SampleElement> getElements() {
            return (LinkedList<SampleElement>) elements.clone();
        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            if (qName.equalsIgnoreCase("id")) {
                tagId = false;
            } else if (qName.equalsIgnoreCase("name")) {
                tagName = false;
            } else if (qName.equalsIgnoreCase("value")) {
                tagValue = false;

                // create new SampleElement
                if (id != null && name != null && value != null) {
                    int val = Integer.parseInt(value);
                    SampleElement element = new SampleElement(id, name, val);
                    elements.add(element);

                    // clear strings
                    id = name = value = null;
                }
            }
        }

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            if (qName.equalsIgnoreCase("id")) {
                tagId = true;
                id = null;
            } else if (qName.equalsIgnoreCase("name")) {
                tagName = true;
                name = null;
            } else if (qName.equalsIgnoreCase("value")) {
                tagValue = true;
                value = null;
            }
        }
    }
}
